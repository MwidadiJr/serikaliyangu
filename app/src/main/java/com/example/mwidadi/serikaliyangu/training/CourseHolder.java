package com.example.mwidadi.serikaliyangu.training;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class CourseHolder extends RecyclerView.ViewHolder {
    ImageView cover_image;
    TextView time;
    TextView title;
    Button bn_read;

    public CourseHolder(View itemView) {
        super(itemView);
        cover_image = (ImageView) itemView.findViewById(R.id.iv_image);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        time = (TextView) itemView.findViewById(R.id.tv_time);
        bn_read = (Button)itemView.findViewById(R.id.bn_read);
    }
}
