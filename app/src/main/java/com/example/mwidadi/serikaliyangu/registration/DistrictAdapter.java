package com.example.mwidadi.serikaliyangu.registration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rabson on 1/4/2017.
 */
public class DistrictAdapter extends BaseAdapter {

    Context c;
    List<District> districtList= Collections.emptyList();
    LayoutInflater inflater;

    public DistrictAdapter(Context c, List<District> districtList) {
        this.c = c;
        this.districtList = districtList;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return districtList.size();
    }

    @Override
    public Object getItem(int position) {
        return districtList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.single_spinner_row, parent, false);
        }
        TextView servicename = (TextView) convertView.findViewById(R.id.tvServiceName);
        ImageView serviceicon = (ImageView) convertView.findViewById(R.id.ivServiceLogo);
        District locations = districtList.get(position);
        servicename.setText(String.valueOf(locations.getDistrict_name()));
        Picasso.with(c).load(locations.getRegion_id())
                .placeholder(R.drawable.ic_action_location)
                .into(serviceicon);
        return convertView;
    }
}
