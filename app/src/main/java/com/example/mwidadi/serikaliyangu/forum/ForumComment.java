package com.example.mwidadi.serikaliyangu.forum;

/**
 * Created by Mwidadi on 11/28/2017.
 */

public class ForumComment {
    private String comment_content;
    private String comment_posted;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }

    public String getComment_posted() {
        return comment_posted;
    }

    public void setComment_posted(String comment_posted) {
        this.comment_posted = comment_posted;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }
}
