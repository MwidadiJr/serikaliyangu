package com.example.mwidadi.serikaliyangu.utils;
import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceManager {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context mContext;
    private static final int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "basic_user_information";
    private static final String USER_ID = "user_id";
    private static final String FIRST_NAME = "first_name";
    private static final String MIDDLE_NAME = "middle_name";
    private static final String LAST_NAME = "last_name";
    private static final String AVATAR = "avatar";
    private static final String CREATED_AT = "created_at";
    private static final String UPDATED_AT = "updated_at";
    private static final String IS_LOGGED_IN = "loggedIn";
    private static final String EMAIL = "email";
    private static final String PROFILE_ID = "profile_id";
    private static final String BIRTH_DAY = "birth_day";
    private static final String GENDER= "gender";
    private static final String MARITAL_STATUS = "marital_status";
    private static final String JOB_STATUS = "job_status";
    private static final String PHONE = "phone";
    private static final String JOB_TITLE = "job_title";
    private static final String PASSWORD = "password";
    private static final String DISTRICT_ID = "district_id";
    private static final String REGION_ID = "region_id";
    private static final String REGION_NAME = "region_name";
    private static final String DISTRICT_NAME = "district_name";

    public PreferenceManager(Context context) {
        this.mContext = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();

    }

    public void createLogin(String phone, String user_id) {
        editor.putString(PHONE, phone);
        editor.putString(USER_ID, user_id);
        editor.commit();
    }

    public void setIsLoggedIn(boolean loggedIn){
        editor.putBoolean(IS_LOGGED_IN,loggedIn);
        editor.commit();
    }

    public boolean getLoggedIn(){
        return preferences.getBoolean(IS_LOGGED_IN,false);
    }


    public void setUserId(String userId){
        editor.putString(USER_ID,userId);
        editor.commit();
    }

    public String getUserId() {
        return preferences.getString(USER_ID, null);
    }

    public void setFirstName(String firstName){
        editor.putString(FIRST_NAME,firstName);
        editor.commit();
    }

    public String getFirstName() {
        return preferences.getString(FIRST_NAME, null);
    }

    public void setMiddleName(String middleName){
        editor.putString(MIDDLE_NAME,middleName);
        editor.commit();
    }

    public String getMiddleName() {
        return preferences.getString(MIDDLE_NAME, null);
    }

    public void setLastName(String lastName){
        editor.putString(LAST_NAME,lastName);
        editor.commit();
    }

    public String getLastName() {
        return preferences.getString(LAST_NAME, null);
    }

    public void setBirthDay(String birthDay){
        editor.putString(BIRTH_DAY,birthDay);
        editor.commit();
    }

    public String getBirthDay() {
        return preferences.getString(BIRTH_DAY, null);
    }

    public void setAvatar(String avatar){
        editor.putString(AVATAR,avatar);
        editor.commit();
    }

    public String getAvatar() {
        return preferences.getString(AVATAR, null);
    }

    public void setGender(String gender){
        editor.putString(GENDER,gender);
        editor.commit();
    }

    public String getGender() {
        return preferences.getString(GENDER, null);
    }

    public void setPhone(String phone){
        editor.putString(PHONE,phone);
        editor.commit();
    }

    public String getPhone() {
        return preferences.getString(PHONE, null);
    }

    public void setCreatedAt(String createdAt){
        editor.putString(CREATED_AT,createdAt);
        editor.commit();
    }

    public String getCreatedAt() {
        return preferences.getString(CREATED_AT, null);
    }

    public void setUpdatedAt(String updatedAt){
        editor.putString(UPDATED_AT,updatedAt);
        editor.commit();
    }

    public String getUpdatedAt() {
        return preferences.getString(UPDATED_AT, null);
    }

    public void setMaritalStatus(String maritalStatus){
        editor.putString(MARITAL_STATUS,maritalStatus);
        editor.commit();
    }

    public String getMaritalStatus() {
        return preferences.getString(MARITAL_STATUS, null);
    }

    public void setJobStatus(String jobStatus){
        editor.putString(JOB_STATUS,jobStatus);
        editor.commit();
    }

    public String getJobStatus() {
        return preferences.getString(JOB_STATUS, null);
    }

    public void setEmail(String email){
        editor.putString(EMAIL,email);
        editor.commit();
    }

    public String getEmail() {
        return preferences.getString(EMAIL, null);
    }

    public void setProfileId(String profileId){
        editor.putString(PROFILE_ID,profileId);
        editor.commit();
    }

    public String getProfileId() {
        return preferences.getString(PROFILE_ID, null);
    }

    public void setJobTitle(String jobTitle){
        editor.putString(JOB_TITLE,jobTitle);
        editor.commit();
    }

    public String getJobTitle() {
        return preferences.getString(JOB_TITLE, null);
    }

    public void setPassword(String password){
        editor.putString(PASSWORD,password);
        editor.commit();
    }

    public String getPassword() {
        return preferences.getString(PASSWORD, null);
    }

    public void setRegionId(String regionId){
        editor.putString(REGION_ID,regionId);
        editor.commit();
    }

    public String getRegionId(){
        return preferences.getString(REGION_ID,null);
    }

    public void setDistrictId(String districtId){
        editor.putString(DISTRICT_ID,districtId);
        editor.commit();
    }

    public String getDistrictId(){
        return preferences.getString(DISTRICT_ID,null);
    }

    public void setRegionName(String regionName){
        editor.putString(REGION_NAME,regionName);
        editor.commit();
    }

    public String getRegionName(){
        return preferences.getString(REGION_NAME,null);
    }

    public void setDistrictName(String districtName){
        editor.putString(DISTRICT_NAME,districtName);
        editor.commit();
    }

    public String getDistrictName(){
        return preferences.getString(DISTRICT_NAME,null);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
