package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class GroupHolder extends RecyclerView.ViewHolder {
    TextView description;
    TextView title;
    TextView location;
    CardView cvCard;

    public GroupHolder(View itemView) {
        super(itemView);
        description = (TextView) itemView.findViewById(R.id.tv_description);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        location = (TextView) itemView.findViewById(R.id.tv_location);
        cvCard = (CardView) itemView.findViewById(R.id.cv_group);
    }
}
