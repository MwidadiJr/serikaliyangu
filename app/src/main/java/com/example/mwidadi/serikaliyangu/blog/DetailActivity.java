package com.example.mwidadi.serikaliyangu.blog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;
import java.io.File;
import java.io.FileOutputStream;
import me.anwarshahriar.calligrapher.Calligrapher;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView iv_image,iv_share;
    private TextView tv_title,tv_description,tv_sender,tv_views,tv_time;
    private CardView cv_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.text_details_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                }
            });

        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        initView();
        getAndSet();
    }

    private void initView(){
        iv_image = (ImageView) findViewById(R.id.iv_image);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_sender = (TextView) findViewById(R.id.tv_sender);
        tv_views = (TextView) findViewById(R.id.tv_views);
        tv_time = (TextView) findViewById(R.id.tv_time);
        iv_share = (ImageView)findViewById(R.id.iv_share);
        cv_news = (CardView)findViewById(R.id.cv_news);
        iv_share.setOnClickListener(this);
    }

    private void getAndSet(){
        Intent i = this.getIntent();
        String title = i.getExtras().getString("TITTLE_KEY");
        String date = i.getExtras().getString("DATE_KEY");
        String content = i.getExtras().getString("CONTENT_KEY");
        String sender = i.getExtras().getString("SENDER_KEY");
        String views = i.getExtras().getString("VIEW_KEY");
        int image = i.getExtras().getInt("IMAGE_KEY");

        iv_image.setImageResource(image);
        tv_time.setText(date);
        tv_description.setText(content);
        tv_title.setText(title);
        tv_sender.setText(sender);
        tv_views.setText(views);
    }

    @Override
    public void onClick(View v) {
       if (v == iv_share){
          shareNews();
       }
    }

    private void shareNews(){
        Bitmap bitmap = getBitmapFromView(cv_news);
        try {
            File file = new File(getCacheDir(), "logicchis" + ".png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share this news via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null) {
            bgDrawable.draw(canvas);
        }   else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }
}
