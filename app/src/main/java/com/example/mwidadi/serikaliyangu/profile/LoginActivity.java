package com.example.mwidadi.serikaliyangu.profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.MainActivity;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.registration.BasicRegisterActivity;
import com.example.mwidadi.serikaliyangu.registration.ProfileRegistrationActivity;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import me.anwarshahriar.calligrapher.Calligrapher;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText et_email,et_password;
    private ImageView iv_profile;
    private TextView tv_forget_password,tv_register;
    private Button bn_login;
    private CheckBox cb_show_password;
    private Context mContext;
    private ProgressBar mProgressBar;
    private String email_address;
    PreferenceManager preferenceManager;
    private String user_id;
    private String phone_number;
    private String email;
    private String user_created;
    private String user_updated;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String user_dob;
    private String user_gender;
    private String user_marital_status;
    private String user_job_status;
    private String user_job_title;
    private String profile_created;
    private String profile_updated;
    private String phone,password;
    private String format_phone;
    private String full_avatar_url = "";
    private String district_id;
    private String region_id;
    private String region_name;
    private Button bn_submit;
    private EditText et_reset_password;
    String[] admin_email = {"mwidadi@thinkerstech.com"};
    private String district_name;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        mContext = LoginActivity.this;
        preferenceManager = new PreferenceManager(this);
        checkIfLoggedIn();
        initViews();
    }

    private void initViews(){
        et_email = (EditText) findViewById(R.id.et_phone);
        et_password = (EditText) findViewById(R.id.et_password);
        tv_forget_password = (TextView) findViewById(R.id.tv_forget);
        tv_register = (TextView) findViewById(R.id.tv_register);
        bn_login = (Button) findViewById(R.id.bn_login);
        iv_profile = (ImageView) findViewById(R.id.iv_profile);
        bn_login.setOnClickListener(this);
        tv_forget_password.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        cb_show_password = (CheckBox) findViewById(R.id.cbRemember);
        cb_show_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == bn_login){
             validate();
        }

        else if (v == tv_forget_password){
          getDialog();
        }

        else if (v ==  tv_register){
            Intent intent = new Intent(getApplicationContext(),BasicRegisterActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        }
    }

    private void userLogin(final String phone, final String password){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.text_login_label));
        progressDialog.setMessage(getString(R.string.text_login_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest loginRequest = new StringRequest(Request.Method.POST, APIConfig.USER_LOGIN_URL, new Response.Listener<String>() {
            JSONObject userObject = null;
            JSONObject profileObject = null;
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                if (response.contains("unazingua")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_login), Toast.LENGTH_SHORT).show();
                }
                else {
                    try {
                        userObject = new JSONObject(response);
                        JSONObject singleUserObject = userObject.getJSONObject("user");
                            user_id = singleUserObject.getString("id");
                            phone_number = singleUserObject.getString("phone");
                            email = singleUserObject.getString("email");
                            user_created = singleUserObject.getString("created_at");
                            user_updated = singleUserObject.getString("updated_at");

                            profileObject = singleUserObject.getJSONObject("profile");
                            profile_id = profileObject.getString("id");
                            first_name = profileObject.getString("first_name");
                            middle_name = profileObject.getString("middle_name");
                            last_name = profileObject.getString("last_name");
                            user_avatar = profileObject.getString("avatar");
                            user_dob = profileObject.getString("dob");
                            user_gender = profileObject.getString("gender");
                            user_marital_status = profileObject.getString("marital_status");
                            user_job_status = profileObject.getString("job_status");
                            user_job_title = profileObject.getString("job_title");
                            profile_created = profileObject.getString("created_at");
                            profile_updated = profileObject.getString("updated_at");
                            full_avatar_url = APIConfig.PREPEND_STORAGE_URL+user_avatar;

                         JSONObject districtObject = profileObject.getJSONObject("district");
                           district_id = districtObject.getString("id");
                           region_id = districtObject.getString("region_id");
                           district_name = districtObject.getString("name");

                        JSONObject regionObject = districtObject.getJSONObject("region");
                          region_id = regionObject.getString("id");
                          region_name = regionObject.getString("name");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (isUserExist(user_id)){
                        Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
                    }
                    else if (String.valueOf(profileObject).equalsIgnoreCase("null")){
                        setSession();
                        Intent intent = new Intent(getApplicationContext(),ProfileRegistrationActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                    }
                    else {
                        setSession();
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_PHONE,phone);
                params.put(APIConfig.KEY_PASSWORD,password);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        loginRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(loginRequest);
    }

    private void validate(){
        phone = et_email.getText().toString().trim();
        password = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(phone)){
            Toast.makeText(getApplicationContext(),"Phone number field cannot be empty!",Toast.LENGTH_SHORT).show();
        }
        else if(!phone.substring(0,4).equalsIgnoreCase("+255")){
            Toast.makeText(getApplicationContext(),"Phone number format must begin with +255",Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(password)){
            Toast.makeText(getApplicationContext(),"Password field cannot be empty!",Toast.LENGTH_SHORT).show();
        }
        else {
            userLogin(phone,password);
        }
    }

    private boolean isUserExist(String user_id){
        if (user_id.length()<0){
            return true;
        }
        else {
            return false;
        }
    }

    private boolean isStringNull(String string){
        if (string.equals("")){
            return true;
        }
        else {
            return false;
        }
    }

    private void setSession(){
        preferenceManager = new PreferenceManager(this);
        preferenceManager.createLogin(phone_number,user_id);
        preferenceManager.setIsLoggedIn(true);
        preferenceManager.setUserId(user_id);
        preferenceManager.setFirstName(first_name);
        preferenceManager.setMiddleName(middle_name);
        preferenceManager.setLastName(last_name);
        preferenceManager.setPhone(phone);
        preferenceManager.setEmail(email);
        preferenceManager.setAvatar(user_avatar);
        preferenceManager.setBirthDay(user_dob);
        preferenceManager.setDistrictId(district_id);
        preferenceManager.setRegionId(region_id);
        preferenceManager.setDistrictName(district_name);
        preferenceManager.setRegionName(region_name);
        preferenceManager.setGender(user_gender);
        preferenceManager.setMaritalStatus(user_marital_status);
        preferenceManager.setJobTitle(user_job_title);
        preferenceManager.setJobStatus(user_job_status);
        preferenceManager.setProfileId(profile_id);
    }

    private void checkIfLoggedIn(){
        if (preferenceManager.getLoggedIn()){
            Intent mainIntent = new Intent(LoginActivity.this,MainActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainIntent);
            finish();
        }
    }

    private void getDialog(){
        final Dialog createDialog = new Dialog(this);
        createDialog.setContentView(R.layout.reset_password_dialog);
        bn_submit = (Button) createDialog.findViewById(R.id.bn_submit);
        et_reset_password = (EditText) createDialog.findViewById(R.id.et_email);

        bn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = et_reset_password.getText().toString().trim();
                if (TextUtils.isEmpty(email)){
                    Toast.makeText(mContext,"Cannot be empty",Toast.LENGTH_SHORT).show();
                }
                else {
                     sendReset(email);
                }
                createDialog.dismiss();
            }
        });

        createDialog.show();
    }

    public void sendReset(final String email_address) {
        Intent helpIntent = new Intent(Intent.ACTION_SEND);
        helpIntent.setType("rfc/822");
        helpIntent.putExtra(Intent.EXTRA_EMAIL, admin_email);
        helpIntent.putExtra(Intent.EXTRA_SUBJECT, "Tanzania Yangu reset password request from: " + email_address);
        helpIntent.putExtra(Intent.EXTRA_TEXT, email_address + "\n\nReplying Email is :" + email_address);
        startActivity(helpIntent);
    }

}
