package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.chat.User;
import com.example.mwidadi.serikaliyangu.chat.UserAdapter;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.StringFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllChatFragment extends Fragment {

    private RecyclerView rv_chat;
    private String user_id;
    private String district_id;
    private String phone;
    private String email;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String avatar;
    private String district_name;
    private String region_name;
    private String full_avatar_url = "";
    private ArrayList<User> userData = new ArrayList<>();
    private ProgressBar pBar;
    private StringFormatter stringFormatter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_chat, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        rv_chat = (RecyclerView) v.findViewById(R.id.rv_chat);
        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        stringFormatter = new StringFormatter(getContext());
        rv_chat.setLayoutManager(new LinearLayoutManager(getContext()));
        getAllUsers();
        return v;

    }

    private void getAllUsers(){
        pBar.setVisibility(View.VISIBLE);
        StringRequest usersRequest = new StringRequest(Request.Method.GET, APIConfig.GET_ALL_USERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pBar.setVisibility(View.INVISIBLE);
                try {
                    JSONObject userObject = new JSONObject(response);
                    JSONArray userArray = userObject.getJSONArray("users");
                    for (int i = 0;i<userArray.length();i++){
                        JSONObject allUserObject = userArray.getJSONObject(i);
                        User user = new User();
                        user_id = allUserObject.getString("id");
                        phone = allUserObject.getString("phone");
                        email = allUserObject.getString("email");

                        JSONObject profileObject = allUserObject.getJSONObject("profile");
                        profile_id = profileObject.getString("id");
                        user_id = profileObject.getString("id");
                        district_id = profileObject.getString("district_id");
                        first_name = profileObject.getString("first_name");
                        middle_name = profileObject.getString("middle_name");
                        last_name = profileObject.getString("last_name");
                        avatar = profileObject.getString("avatar");

                        full_avatar_url = APIConfig.PREPEND_STORAGE_URL+avatar;

                        JSONObject districtObject = profileObject.getJSONObject("district");
                        district_id = districtObject.getString("id");
                        district_name = stringFormatter.properCase(districtObject.getString("name"));

                        JSONObject regionObject = districtObject.getJSONObject("region");
                        region_name = stringFormatter.properCase(regionObject.getString("name"));

                        user.setUser_id(user_id);
                        user.setPhone(phone);
                        user.setEmail(email);
                        user.setProfile_id(profile_id);
                        user.setDistrict_id(district_id);
                        user.setFirst_name(first_name);
                        user.setMiddle_name(middle_name);
                        user.setLast_name(last_name);
                        user.setAvatar(full_avatar_url);
                        user.setRegion_name(region_name);
                        user.setDistrict_name(district_name);

                        userData.add(user);
                    }

                    UserAdapter adapter = new UserAdapter(getContext(), userData);
                    rv_chat.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.INVISIBLE);
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getActivity(). getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        usersRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(usersRequest);
    }
}
