package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import me.anwarshahriar.calligrapher.Calligrapher;

public class MemberListActivity extends AppCompatActivity {

    private String group_id;
    private ArrayList<MemberModel> memberData = new ArrayList<>();
    private RecyclerView rv_member;
    private String image_path = "";
    private ProgressBar pBar;
    private String total_member;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.member_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change, R.anim.slide_down);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        rv_member = (RecyclerView) findViewById(R.id.rv_member);
        pBar = (ProgressBar) findViewById(R.id.progressBar);
        rv_member.setLayoutManager(new LinearLayoutManager(this));
        rv_member.setHasFixedSize(true);
        getGroupInfo();
        getMemberList();
    }

    private void getGroupInfo(){
        group_id = getIntent().getStringExtra("group_id");
    }

    private void getMemberList(){
        pBar.setVisibility(View.VISIBLE);
        StringRequest groupRequest = new StringRequest(Request.Method.GET, APIConfig.GROUP_URL+"/"+group_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject mainGroupObject = new JSONObject(response);
                    pBar.setVisibility(View.GONE);
                    JSONObject groupObject = mainGroupObject.getJSONObject("group");
                    JSONArray memberArray = groupObject.getJSONArray("group_users");
                    total_member = String.valueOf(memberArray.length());
                    for (int i = 0;i<memberArray.length();i++){
                        JSONObject singleMemberObject = memberArray.getJSONObject(i);
                        MemberModel member = new MemberModel();
                        String member_user_id = singleMemberObject.getString("id");
                        String member_phone = singleMemberObject.getString("phone");
                        String member_email = singleMemberObject.getString("email");

                        JSONObject memberProfileObject = singleMemberObject.getJSONObject("profile");
                        String member_first_name = memberProfileObject.getString("first_name");
                        String member_middle_name = memberProfileObject.getString("middle_name");
                        String member_last_name = memberProfileObject.getString("last_name");
                        String member_avatar = memberProfileObject.getString("avatar");
                        String member_gender = memberProfileObject.getString("gender");

                        String proper_gender = properCase(member_gender);
                        image_path = APIConfig.PREPEND_STORAGE_URL+member_avatar;

                        member.setUser_id(member_user_id);
                        member.setPhone(member_phone);
                        member.setEmail(member_email);
                        member.setFirst_name(member_first_name);
                        member.setMiddle_name(member_middle_name);
                        member.setLast_name(member_last_name);
                        member.setAvatar(image_path);
                        member.setGender(proper_gender);

                        memberData.add(member);
                    }

                    MemberAdapter adapter = new MemberAdapter(getApplicationContext(),memberData);
                    rv_member.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        groupRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(groupRequest);
    }

    public String properCase(String paramString) {
        if (paramString.length() == 0) {
            return "";
        }
        if (paramString.length() == 1) {
            return paramString.toUpperCase();
        }
        return paramString.substring(0, 1).toUpperCase() + paramString.substring(1).toLowerCase();
    }
}
