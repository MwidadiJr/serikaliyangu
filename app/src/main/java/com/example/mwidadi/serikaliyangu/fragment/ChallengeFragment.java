package com.example.mwidadi.serikaliyangu.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.challenge.Challenge;
import com.example.mwidadi.serikaliyangu.challenge.ChallengeAdapter;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeFragment extends Fragment {

    private Toolbar toolbar;
    private String opportunity_id;
    private String user_id;
    private String opportunity_title;
    private String opportunity_post;
    private String opportunity_attachment;
    private String opportunity_created_at;
    private String opportunity_updated_at;
    private String phone;
    private String email;
    private String user_created_time;
    private String user_updated_time;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String dob;
    private String gender;
    private String marital_status;
    private String job_status;
    private String job_title;
    private String ago_time;
    private String full_image_path = "";
    private String full_post_image_path = "";
    private ProgressBar pBar;
    private Context mContext;

    ArrayList<Challenge> challengeData = new ArrayList<>();
    private ChallengeAdapter adapter;
    private CharSequence timePassedString;
    private RecyclerView rv_challenge;
    private String opportunity_deadline;
    private String opportunity_category;
    private String opportunity_award;
    private Date date;
    private Date deadline;

    public ChallengeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_challenge, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        setHasOptionsMenu(true);
        rv_challenge = (RecyclerView) v.findViewById(R.id.rv_challenge);
        rv_challenge.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        pBar = (ProgressBar)v.findViewById(R.id.progressBar);
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.challenge_label);
        getOpportunity(APIConfig.GET_OPPORTUNITY_URL);
        mContext = getActivity();
        return v;
    }

    private void getOpportunity(final String get_forum_url) {
        StringRequest challengeRequest = new StringRequest(Request.Method.GET, get_forum_url, new Response.Listener<String>() {
            JSONObject challengeObject = null;

            @Override
            public void onResponse(String response) {
                pBar.setVisibility(View.GONE);
                try {
                    challengeObject = new JSONObject(response);
                    challengeData.clear();
                    JSONArray challengeArray = challengeObject.getJSONArray("opportunities");
                    for (int i = 0; i < challengeArray.length(); i++) {
                        challengeObject = challengeArray.getJSONObject(i);
                        Challenge challenge = new Challenge();
                        opportunity_id = challengeObject.getString("id");
                        user_id = challengeObject.getString("user_id");
                        opportunity_title = challengeObject.getString("title");
                        opportunity_post = challengeObject.getString("post");
                        opportunity_attachment = challengeObject.getString("attachment");
                        opportunity_deadline = challengeObject.getString("deadline");
                        opportunity_category = challengeObject.getString("category");
                        opportunity_award = challengeObject.getString("award");
                        opportunity_created_at = challengeObject.getString("created_at");
                        opportunity_updated_at = challengeObject.getString("updated_at");
                        full_post_image_path = APIConfig.PREPEND_STORAGE_URL+opportunity_attachment;
                        deadline = stringToDate(opportunity_deadline);
                        String day = convertDate(deadline);
                        String month = convertMonth(deadline);
                        String year = convertYear(deadline);

                        JSONObject userObject = challengeObject.optJSONObject("user");
                        user_id = userObject.getString("id");
                        phone = userObject.getString("phone");
                        email = userObject.getString("email");
                        user_created_time = userObject.getString("created_at");
                        user_updated_time = userObject.getString("updated_at");

                        JSONObject profileObject = userObject.getJSONObject("profile");
                        profile_id = profileObject.getString("id");
                        user_id = profileObject.getString("user_id");
                        first_name = profileObject.getString("first_name");
                        middle_name = profileObject.getString("middle_name");
                        last_name = profileObject.getString("last_name");
                        user_avatar = profileObject.getString("avatar");
                        dob = profileObject.getString("dob");
                        gender = profileObject.getString("gender");
                        marital_status = profileObject.getString("marital_status");
                        job_status = profileObject.getString("job_status");
                        job_title = profileObject.getString("job_title");
                        full_image_path = APIConfig.PREPEND_STORAGE_URL+user_avatar;

                        CharSequence formatedTime = formatTime(opportunity_created_at);
                        ago_time = formatedTime.toString();

                        String challenge_category = properCase(opportunity_category);

                        challenge.setOpportunity_id(opportunity_id);
                        challenge.setOpportunity_title(opportunity_title);
                        challenge.setOpportunity_content(opportunity_post);
                        challenge.setOpportunity_attachment(full_post_image_path);
                        challenge.setPosted_time(ago_time);
                        challenge.setUser_id(user_id);
                        challenge.setFirst_name(first_name);
                        challenge.setMiddle_name(middle_name);
                        challenge.setLast_name(last_name);
                        challenge.setUser_avatar(full_image_path);
                        challenge.setDeadline(opportunity_deadline);
                        challenge.setAward(opportunity_award);
                        challenge.setCategory(challenge_category);
                        challenge.setChallenge_date(day);
                        challenge.setChallenge_month(month);
                        challenge.setChallenge_year(year);
                        challengeData.add(challenge);
                    }

                    adapter = new ChallengeAdapter(getContext(), challengeData);
                    rv_challenge.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                Snackbar.make(rv_challenge,R.string.no_internet,Snackbar.LENGTH_INDEFINITE).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                getOpportunity(APIConfig.GET_OPPORTUNITY_URL);
                    }
                }).show();
                pBar.setVisibility(View.INVISIBLE);
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        challengeRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(challengeRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.challenge_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()== R.id.action_refresh){
            if (pBar.getVisibility() == View.INVISIBLE){
                pBar.setVisibility(View.VISIBLE);
            }
            refresh();
        }
        return super.onOptionsItemSelected(item);
    }

    private CharSequence formatTime(String dateTime){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date  = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString (time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }

    private String properCase(String paramString) {
        if (paramString.length() == 0) {
            return "";
        }
        if (paramString.length() == 1) {
            return paramString.toUpperCase();
        }
        return paramString.substring(0, 1).toUpperCase() + paramString.substring(1).toLowerCase();
    }

    private String convertDate(Date date){
        String day = (String) DateFormat.format("dd",   date); // 20
        return  day;
    }

    private String convertMonth(Date date){
        String monthString  = (String) DateFormat.format("MMM",  date); // Jun
        return  monthString;
    }

    private String convertYear(Date date){
        String year  = (String) DateFormat.format("yyyy", date); // 2013
        return  year;
    }

    private Date stringToDate(String stringDate){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return  date;
    }

    public void refresh() {
        if (this.challengeData != null) {
            this.challengeData.clear();
            getOpportunity(APIConfig.GET_OPPORTUNITY_URL);
            return;
        }
        else{
            getOpportunity(APIConfig.GET_OPPORTUNITY_URL);
        }
    }

}
