package com.example.mwidadi.serikaliyangu.training;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class TrainingCommentActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText et_comment;
    private RecyclerView rv_comment;
    private ImageButton iv_send;
    private CircleImageView chat_add_btn;
    private String comment_id;
    private String user_id;
    private String comment_content;
    private String created_at;
    private String updated_at;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String training_id;
    private ArrayList<TrainingComment> commentData = new ArrayList<>();
    private String full_avatar_url = "";
    private TrainingCommentAdapter adapter;
    private TrainingComment comment;
    private PreferenceManager preferenceManager;
    private String received_first_name;
    private String received_middle_name;
    private String received_last_name;
    private String received_user_id;
    private String received_user_avatar;
    private String full_user_avatar_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.comment_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change,R.anim.slide_down);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        training_id = getIntent().getStringExtra("course_id");
        preferenceManager = new PreferenceManager(this);
        received_first_name = preferenceManager.getFirstName();
        received_middle_name = preferenceManager.getMiddleName();
        received_last_name = preferenceManager.getLastName();
        received_user_id = preferenceManager.getUserId();
        received_user_avatar = preferenceManager.getAvatar();
        full_user_avatar_url = APIConfig.PREPEND_STORAGE_URL+received_user_avatar;
        initView();
        getCourseComment();
    }

    private void initView(){
        et_comment = (EditText) findViewById(R.id.et_comment);
        iv_send = (ImageButton) findViewById(R.id.iv_send);
        rv_comment = (RecyclerView) findViewById(R.id.rv_comment);
        chat_add_btn = (CircleImageView) findViewById(R.id.chat_add_btn);
        Picasso.with(getApplicationContext()).load(full_user_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(chat_add_btn);
        rv_comment.setLayoutManager(new LinearLayoutManager(this));
        iv_send.setOnClickListener(this);
    }

    private void getCourseComment(){
       StringRequest commentRequest = new StringRequest(Request.Method.GET, APIConfig.GET_COURSE_COMMENT_URL+training_id, new Response.Listener<String>() {
           @Override
           public void onResponse(String response) {
               try {
                   JSONObject commentObject = new JSONObject(response);
                   JSONObject singleCommentObject = commentObject.getJSONObject("course");
                   JSONArray commentArray = singleCommentObject.getJSONArray("comments");
                   for (int i = 0;i<commentArray.length();i++){
                       JSONObject mainCommentObject = commentArray.getJSONObject(i);
                       TrainingComment training = new TrainingComment();
                       String comment_id = mainCommentObject.getString("id");
                       String course_id = mainCommentObject.getString("course_id");
                       String user_id = mainCommentObject.getString("user_id");
                       String comment = mainCommentObject.getString("comment");
                       String created_at = mainCommentObject.getString("created_at");
                       String updated_at = mainCommentObject.getString("updated_at");

                       JSONObject userObject = mainCommentObject.getJSONObject("user");
                       JSONObject profileObject = userObject.getJSONObject("profile");
                       String first_name = profileObject.getString("first_name");
                       String middle_name = profileObject.getString("middle_name");
                       String last_name = profileObject.getString("last_name");
                       String user_avatar = profileObject.getString("avatar");

                       full_avatar_url = APIConfig.PREPEND_STORAGE_URL+user_avatar;

                       training.setComment_id(comment_id);
                       training.setCourse_id(course_id);
                       training.setUser_id(user_id);
                       training.setComment_content(comment);
                       training.setCreated_at(created_at);
                       training.setUpdated_at(updated_at);
                       training.setFirst_name(first_name);
                       training.setMiddle_name(middle_name);
                       training.setLast_name(last_name);
                       training.setUser_avatar(full_avatar_url);

                       commentData.add(training);
                   }

                   TrainingCommentAdapter adapter = new TrainingCommentAdapter(getApplicationContext(),commentData);
                   rv_comment.setAdapter(adapter);
                   adapter.notifyDataSetChanged();

               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               if (error instanceof com.android.volley.TimeoutError){
                   LayoutInflater inflater = getLayoutInflater();
                   View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                   Toast toast = new Toast(getApplicationContext());
                   toast.setDuration(Toast.LENGTH_LONG);
                   toast.setView(toastLayout);
                   toast.show();

               }
               else if (error instanceof com.android.volley.AuthFailureError){
                   LayoutInflater inflater = getLayoutInflater();
                   View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                   Toast toast = new Toast(getApplicationContext());
                   toast.setDuration(Toast.LENGTH_LONG);
                   toast.setView(toastLayout);
                   toast.show();
               }
               else if (error instanceof com.android.volley.NetworkError){
                   LayoutInflater inflater = getLayoutInflater();
                   View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                   Toast toast = new Toast(getApplicationContext());
                   toast.setDuration(Toast.LENGTH_LONG);
                   toast.setView(toastLayout);
                   toast.show();
               }
               else if (error instanceof  com.android.volley.ServerError){
                   LayoutInflater inflater = getLayoutInflater();
                   View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                   Toast toast = new Toast(getApplicationContext());
                   toast.setDuration(Toast.LENGTH_LONG);
                   toast.setView(toastLayout);
                   toast.show();
               }
               else if (error instanceof com.android.volley.ParseError){
                   LayoutInflater inflater = getLayoutInflater();
                   View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                   Toast toast = new Toast(getApplicationContext());
                   toast.setDuration(Toast.LENGTH_LONG);
                   toast.setView(toastLayout);
                   toast.show();
               }
               else if (error instanceof  com.android.volley.NoConnectionError){
                   LayoutInflater inflater = getLayoutInflater();
                   View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                   Toast toast = new Toast(getApplicationContext());
                   toast.setDuration(Toast.LENGTH_LONG);
                   toast.setView(toastLayout);
                   toast.show();
               }
           }
       }){

       };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(commentRequest);
    }

    private void create_comment(final String user_id, final String comment, final String course_id, String comment_url){
        StringRequest commentRequest = new StringRequest(Request.Method.POST, comment_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("comment")){
                    Toast.makeText(getApplicationContext(),"Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Failed please try again",Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_COMMENT,comment);
                params.put(APIConfig.KEY_COURSE_ID,course_id);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(commentRequest);
    }

    private void hideSoftKeyboard(){
        if (getCurrentFocus() != null){
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == iv_send){
                comment = new TrainingComment();
                if (received_user_id != null){
                    if (commentData!= null){
                        comment.setComment_content(et_comment.getText().toString().trim());
                        comment.setFirst_name(received_first_name);
                        comment.setLast_name(received_last_name);
                        comment.setUser_avatar(full_user_avatar_url);
                        create_comment(received_user_id,et_comment.getText().toString().trim(),training_id,APIConfig.ADD_COURSE_COMMENT_URL);

                        View focus = getCurrentFocus();
                        if (focus != null) {
                            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                        }

                        et_comment.setText("");
                        hideSoftKeyboard();
                        commentData.add(comment);

                        if (adapter == null) {
                            adapter = new TrainingCommentAdapter(getApplicationContext(),commentData);
                            rv_comment.setAdapter(adapter);

                        } else {
                            adapter.notifyDataSetChanged();
                        }
                    }
                    else {
                        create_comment(received_user_id,et_comment.getText().toString().trim(),training_id,APIConfig.ADD_COURSE_COMMENT_URL);
                        et_comment.setText("");
                        hideSoftKeyboard();
                 }
            }

        }
    }
}
