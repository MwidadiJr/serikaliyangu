package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.APIPostRequest;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ForumCommentActivity extends AppCompatActivity implements View.OnClickListener {

    private String forum_id;
    ArrayList<ForumComment> commentData = new ArrayList<>();
    private CommentAdapter adapter;
    private RecyclerView rv_comment;
    private APIPostRequest apiPostRequest;
    private EditText et_comment;
    private ImageView iv_send;
    private CircleImageView chat_add_btn;
    private ForumComment commentModel;
    private String user_id;
    private String full_image_path = "";
    private String full_avatar_url = "";
    private PreferenceManager preferenceManager;
    private String first_name,middle_name,last_name,user_avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_comment);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.comment_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   finish();
                    overridePendingTransition(R.anim.no_change,R.anim.slide_down);
                }
            });
        }
        preferenceManager = new PreferenceManager(this);
        user_id = preferenceManager.getUserId();
        first_name = preferenceManager.getFirstName();
        last_name = preferenceManager.getLastName();
        middle_name = preferenceManager.getMiddleName();
        user_avatar = preferenceManager.getAvatar();
        full_avatar_url = APIConfig.PREPEND_STORAGE_URL+user_avatar;
        et_comment = (EditText) findViewById(R.id.et_comment);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        chat_add_btn = (CircleImageView) findViewById(R.id.chat_add_btn);
        Picasso.with(getApplicationContext()).load(full_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(chat_add_btn);
        iv_send.setOnClickListener(this);
        forum_id = getIntent().getStringExtra("forum_id");
        apiPostRequest = new APIPostRequest(this);
        if (adapter != null){
            getForumComment(forum_id);
        }
        else {
            getForumComment(forum_id);
        }
        rv_comment = (RecyclerView) findViewById(R.id.rv_comment);
        rv_comment.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.no_change,R.anim.slide_down);
    }

    private void getForumComment(final String forum_id){
        final StringRequest commentRequest = new StringRequest(Request.Method.GET, APIConfig.GET_COMMENT_FORUM_URL+forum_id+"/comments", new Response.Listener<String>() {
            JSONObject commentObject = null;
            @Override
            public void onResponse(String response) {
                try {
                    commentObject = new JSONObject(response);
                    JSONObject comments = commentObject.getJSONObject("comments");
                    JSONArray commentArray = comments.getJSONArray("comments");
                    for (int i = 0;i<commentArray.length();i++){
                        JSONObject allCommentObject = commentArray.getJSONObject(i);
                        ForumComment forumComment = new ForumComment();
                        String comment = allCommentObject.getString("comment");
                        String created_at = allCommentObject.getString("created_at");
                        JSONObject userObject = allCommentObject.optJSONObject("user");
                        JSONObject profileObject = userObject.getJSONObject("profile");
                        String first_name = profileObject.getString("first_name");
                        String middle_name = profileObject.getString("middle_name");
                        String last_name = profileObject.getString("last_name");
                        String user_avatar = profileObject.getString("avatar");
                        full_image_path = APIConfig.PREPEND_STORAGE_URL + user_avatar;

                        forumComment.setComment_content(comment);
                        forumComment.setComment_posted(created_at);
                        forumComment.setFirst_name(first_name);
                        forumComment.setMiddle_name(middle_name);
                        forumComment.setLast_name(last_name);
                        forumComment.setUser_avatar(full_image_path);

                        commentData.add(forumComment);
                    }

                    adapter = new CommentAdapter(getApplicationContext(),commentData);
                    adapter.notifyDataSetChanged();
                    rv_comment.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(commentRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == iv_send){
            commentModel = new ForumComment();
            if (user_id != null){
                if (commentData!= null){
                    commentModel.setComment_content(et_comment.getText().toString().trim());
                    commentModel.setFirst_name(first_name);
                    commentModel.setLast_name(last_name);
                    commentModel.setUser_avatar(full_avatar_url);
                    sendComment();

                    View focus = getCurrentFocus();
                    if (focus != null) {
                        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                    }

                    et_comment.setText("");
                    hideSoftKeyboard();
                    commentData.add(commentModel);

                    if (adapter == null) {
                        adapter = new CommentAdapter(getApplicationContext(),commentData);
                        rv_comment.setAdapter(adapter);

                    } else {
                        adapter.notifyDataSetChanged();
                    }
                }
                else {
                    sendComment();
                    et_comment.setText("");
                    hideSoftKeyboard();
                }
            }
        }
    }

    private void sendComment(){
        apiPostRequest.create_forum_comment(user_id,et_comment.getText().toString().trim(),APIConfig.COMMENT_FORUM_URL+forum_id+"/comment");
    }

    private void hideSoftKeyboard(){
        if (getCurrentFocus() != null){
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
    }
}
