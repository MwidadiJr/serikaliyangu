package com.example.mwidadi.serikaliyangu.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.profile.LoginActivity;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.APIPostRequest;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.UserPreferenceManager;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import me.anwarshahriar.calligrapher.Calligrapher;

public class BasicRegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText et_phone,et_email,et_password,et_c_password;
    private Button bn_next;
    private String email,phone,password,c_password;
    private APIPostRequest apiRequest;
    private Context mContext;
    private ProgressBar pBar;
    private UserPreferenceManager userPreferenceManager;
    private PreferenceManager preferenceManager;
    private String user_phone;
    private String user_email;
    private String user_created_at;
    private String user_updated_at;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_registration);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.text_register_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                }
            });
        }
        mContext = BasicRegisterActivity.this;
        apiRequest = new APIPostRequest(mContext);
        pBar = (ProgressBar) findViewById(R.id.progressBar);
        pBar.setVisibility(View.INVISIBLE);
        userPreferenceManager = new UserPreferenceManager(this);
        preferenceManager = new PreferenceManager(this);
        initView();

    }

    private void initView(){
        et_email = (EditText) findViewById(R.id.et_email);
        et_phone = (EditText) findViewById(R.id.et_phone);
        bn_next = (Button) findViewById(R.id.bn_register);
        et_password = (EditText) findViewById(R.id.et_password);
        et_c_password = (EditText) findViewById(R.id.et_confirm_password);
        bn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == bn_next){
            getUserData();
        }
    }

    private void getUserData(){
        email = et_email.getText().toString().trim();
        phone = et_phone.getText().toString().trim();
        password = et_password.getText().toString().trim();
        c_password = et_c_password.getText().toString().trim();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(password) || TextUtils.isEmpty(c_password)){
            Toast.makeText(getApplicationContext(),getString(R.string.empty_field_alert),Toast.LENGTH_SHORT).show();
        }
        else if (!phone.substring(0,4).equalsIgnoreCase("+255")){
            Toast.makeText(getApplicationContext(),"Phone number must begin with +255",Toast.LENGTH_SHORT).show();
        }
        else if (!email.contains("@")){
            Toast.makeText(getApplicationContext(),"Please provide valid email address",Toast.LENGTH_SHORT).show();
        }
        else if(!password.equals(c_password)) {
            Toast.makeText(getApplicationContext(),getString(R.string.password_mismatch_alert),Toast.LENGTH_SHORT).show();
        }
        else {
            registerNewUser(email,phone,password, APIConfig.USER_REGISTRATION_URL,pBar);
            preferenceManager.clearSession();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
        }
    }

    public void registerNewUser(final String email, final String phone, final String password, String api_url, final ProgressBar progressBar){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest registerUserRequest = new StringRequest(Request.Method.POST, api_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject userObject = new JSONObject(response);
                    JSONObject singleUserObject = userObject.getJSONObject("user");
                    user_phone = singleUserObject.getString("phone");
                    user_email = singleUserObject.getString("email");
                    user_created_at = singleUserObject.getString("created_at");
                    user_updated_at = singleUserObject.getString("updated_at");
                    user_id = singleUserObject.getString("id");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_EMAIL,email);
                params.put(APIConfig.KEY_PHONE,phone);
                params.put(APIConfig.KEY_PASSWORD,password);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        registerUserRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(registerUserRequest);
    }

}
