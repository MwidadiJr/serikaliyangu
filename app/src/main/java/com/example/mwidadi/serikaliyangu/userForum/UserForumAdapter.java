package com.example.mwidadi.serikaliyangu.userForum;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class UserForumAdapter extends RecyclerView.Adapter<UserForumHolder> {
    Context context;
    ArrayList<MyForum> forumList = new ArrayList<>();

    public UserForumAdapter(Context context, ArrayList<MyForum> forumList){
        this.forumList = forumList;
        this.context = context;
    }

    @Override
    public UserForumHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_forum,parent,false);
        UserForumHolder forumHolder = new UserForumHolder(v);
        return forumHolder;
    }

    @Override
    public void onBindViewHolder(UserForumHolder holder, int position) {
       final MyForum forum = forumList.get(position);
        holder.content.setText(forum.getPost());
        holder.title.setText(forum.getTitle());
        holder.time.setText(forum.getCreated_at());
        holder.cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), UserForumDetailActivity.class);
                intent.putExtra("forum_data",forum);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.forumList.size();
    }
}
