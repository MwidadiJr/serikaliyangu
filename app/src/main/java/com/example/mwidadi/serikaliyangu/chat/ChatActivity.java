package com.example.mwidadi.serikaliyangu.chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener{

    private PreferenceManager preferenceManager;
    private String user_avatar;
    private User user;
    private String clicked_user_id;
    private String current_user_id;
    private EditText et_comment;
    private CircleImageView cv_image;
    private ImageButton iv_send;
    private String full_image_url = "";
    private ListView lv_chat;
    List<ChatModel> lstChat = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.chat_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change,R.anim.slide_down);
                }
            });
        }
        preferenceManager = new PreferenceManager(this);
        user_avatar = preferenceManager.getAvatar();
        full_image_url = APIConfig.PREPEND_STORAGE_URL+user_avatar;
        lv_chat = (ListView) findViewById(R.id.messages_list);;
        current_user_id = preferenceManager.getUserId();
        user = (User) getIntent().getSerializableExtra("chat_item");
        clicked_user_id = user.getUser_id();
        initView();
    }

    private void initView(){
        et_comment = (EditText) findViewById(R.id.et_comment);
        cv_image = (CircleImageView) findViewById(R.id.chat_add_btn);
        iv_send = (ImageButton) findViewById(R.id.iv_send);
        iv_send.setOnClickListener(this);
        lv_chat = (ListView) findViewById(R.id.messages_list);
        ChatCustomAdpater adapter = new ChatCustomAdpater(lstChat,this);
        lv_chat.setAdapter(adapter);
        Picasso.with(getApplicationContext()).load(full_image_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(cv_image);
    }

    private void sendMessage(){
        StringRequest messageRequest = new StringRequest(Request.Method.POST, APIConfig.CREATE_NEW_THREAD+current_user_id+"/thread", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_RECIPIENT_ID,clicked_user_id);
                params.put(APIConfig.KEY_MESSAGE,et_comment.getText().toString().trim());
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        messageRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(messageRequest);
    }


    @Override
    public void onClick(View v) {
        if (v == iv_send){
            sendMessage();
        }
    }

}
