package com.example.mwidadi.serikaliyangu.training;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class TrainingAdapter extends RecyclerView.Adapter<TrainingHolder> {
    Context context;
    ArrayList<Training> trainingList = new ArrayList<>();

    public TrainingAdapter(Context context, ArrayList<Training> trainingList){
        this.trainingList = trainingList;
        this.context = context;
    }

    @Override
    public TrainingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_training,parent,false);
        TrainingHolder trainingHolder = new TrainingHolder(v);
        return trainingHolder;
    }

    @Override
    public void onBindViewHolder(TrainingHolder holder, int position) {
       final Training training = trainingList.get(position);
        holder.title.setText(training.getTraining_title());
        holder.time.setText(training.getCreated_at());
        holder.tv_sender.setText(training.getFirst_name()+" "+training.getLast_name());
        Picasso.with(this.context.getApplicationContext()).load(training.getAttachment_content())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.cover_image);
        holder.bn_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), TrainingImageDetailActivity.class);
                intent.putExtra("training_item",training);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.trainingList.size();
    }
}
