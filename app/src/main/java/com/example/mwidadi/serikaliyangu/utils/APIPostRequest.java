package com.example.mwidadi.serikaliyangu.utils;

import android.content.Context;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mwidadi on 11/26/2017.
 */

public class APIPostRequest {

    public Context mContext;

    public APIPostRequest(Context context){
        this.mContext = context;
    }

    public void createUserProfile(final String user_id, final String f_name, final String m_name, final String l_name, final String dob, final String gender, final String m_status,
                                  final String job_status, final String job_title, final String avatar,String profile_url){
        StringRequest profileRequest = new StringRequest(Request.Method.POST, profile_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(mContext,response,Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Something went wrong,Please try again!",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_FIRST_NAME,f_name);
                params.put(APIConfig.KEY_MIDDLE_NAME,m_name);
                params.put(APIConfig.KEY_LAST_NAME,l_name);
                params.put(APIConfig.KEY_DOB,dob);
                params.put(APIConfig.KEY_GENDER,gender);
                params.put(APIConfig.KEY_JOB_STATUS,job_status);
                params.put(APIConfig.KEY_JOB_TITLE,job_title);
                params.put(APIConfig.KEY_MARITAL_STATUS,m_status);
                params.put(APIConfig.KEY_PROFILE_AVATAR,avatar);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        profileRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(profileRequest);
    }

    public void createForum(final String user_id,final String title,final String post,final String attachment,String forum_url){
        StringRequest forumRequest = new StringRequest(Request.Method.POST, forum_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(mContext,response,Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Something went wrong,Please try again!",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_TITLE,title);
                params.put(APIConfig.KEY_POST,post);
                params.put(APIConfig.KEY_ATTACHMENT,attachment);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        forumRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(forumRequest);
    }

    public void create_forum_comment(final String user_id, final String comment, String comment_url){
        StringRequest commentRequest = new StringRequest(Request.Method.POST, comment_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("saved")){
                    Toast.makeText(mContext,"Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(mContext,"Failed please try again",Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Something went wrong,Please try again!",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_COMMENT,comment);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(commentRequest);
    }

    public void create_opportunity(final String user_id, final String title, final String post, final String attachment, String opportunity_url){
        StringRequest opportunityRequest = new StringRequest(Request.Method.POST, opportunity_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(mContext,response,Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,"Something went wrong,Please try again!",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_TITLE,title);
                params.put(APIConfig.KEY_POST,post);
                params.put(APIConfig.KEY_ATTACHMENT,attachment);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        opportunityRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(opportunityRequest);
    }

}
