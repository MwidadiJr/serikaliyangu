package com.example.mwidadi.serikaliyangu.challenge;

import java.io.Serializable;

/**
 * Created by Mwidadi on 10/22/2017.
 */

public class Challenge implements Serializable{
   private String opportunity_id;
    private String user_id;
    private String opportunity_title;
    private String opportunity_content;
    private String opportunity_attachment;
    private String posted_time;
    private String updated_time;
    private String user_phone;
    private String user_email;
    private String user_created_time;
    private String user_updated_time;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String user_dob;
    private String user_gender;
    private String user_marital_status;
    private String user_job_status;
    private String job_title;
    private String deadline;
    private String category;
    private String award;
    private String challenge_date;
    private String challenge_month;
    private String challenge_year;

    public String getChallenge_date() {
        return challenge_date;
    }

    public void setChallenge_date(String challenge_date) {
        this.challenge_date = challenge_date;
    }

    public String getChallenge_month() {
        return challenge_month;
    }

    public void setChallenge_month(String challenge_month) {
        this.challenge_month = challenge_month;
    }

    public String getChallenge_year() {
        return challenge_year;
    }

    public void setChallenge_year(String challenge_year) {
        this.challenge_year = challenge_year;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAward() {
        return award;
    }

    public void setAward(String award) {
        this.award = award;
    }

    public String getOpportunity_id() {
        return opportunity_id;
    }

    public void setOpportunity_id(String opportunity_id) {
        this.opportunity_id = opportunity_id;
    }

    public String getOpportunity_title() {
        return opportunity_title;
    }

    public void setOpportunity_title(String opportunity_title) {
        this.opportunity_title = opportunity_title;
    }

    public String getOpportunity_content() {
        return opportunity_content;
    }

    public void setOpportunity_content(String opportunity_content) {
        this.opportunity_content = opportunity_content;
    }

    public String getOpportunity_attachment() {
        return opportunity_attachment;
    }

    public void setOpportunity_attachment(String opportunity_attachment) {
        this.opportunity_attachment = opportunity_attachment;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPosted_time() {
        return posted_time;
    }

    public void setPosted_time(String posted_time) {
        this.posted_time = posted_time;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_created_time() {
        return user_created_time;
    }

    public void setUser_created_time(String user_created_time) {
        this.user_created_time = user_created_time;
    }

    public String getUser_updated_time() {
        return user_updated_time;
    }

    public void setUser_updated_time(String user_updated_time) {
        this.user_updated_time = user_updated_time;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_dob() {
        return user_dob;
    }

    public void setUser_dob(String user_dob) {
        this.user_dob = user_dob;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_marital_status() {
        return user_marital_status;
    }

    public void setUser_marital_status(String user_marital_status) {
        this.user_marital_status = user_marital_status;
    }

    public String getUser_job_status() {
        return user_job_status;
    }

    public void setUser_job_status(String user_job_status) {
        this.user_job_status = user_job_status;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }
}
