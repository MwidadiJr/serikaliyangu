package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class GroupAdapter extends RecyclerView.Adapter<GroupHolder> {
    Context context;
    ArrayList<GroupModel> groupList = new ArrayList<>();

    public GroupAdapter(Context context, ArrayList<GroupModel> groupList){
        this.groupList = groupList;
        this.context = context;
    }

    @Override
    public GroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_group,parent,false);
        GroupHolder group = new GroupHolder(v);
        return group;
    }

    @Override
    public void onBindViewHolder(GroupHolder holder, int position) {
       final GroupModel group = groupList.get(position);
        holder.description.setText(group.getDescription());
        holder.title.setText(group.getTitle());
        holder.location.setText(group.getRegion_name()+" - "+group.getDistrict_name());
        holder.cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), GroupChatActivity.class);
                intent.putExtra("group_item",group);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.groupList.size();
    }
}
