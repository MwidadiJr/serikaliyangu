package com.example.mwidadi.serikaliyangu.challenge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class ChallengeAdapter extends RecyclerView.Adapter<ChallengeHolder> {
    Context context;
    ArrayList<Challenge> challengeList = new ArrayList<>();

    public ChallengeAdapter(Context context, ArrayList<Challenge> challengeList){
        this.challengeList = challengeList;
        this.context = context;
    }

    @Override
    public ChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_challenge,parent,false);
        ChallengeHolder challengeHolder = new ChallengeHolder(v);
        return challengeHolder;
    }

    @Override
    public void onBindViewHolder(ChallengeHolder holder, int position) {
       final Challenge challenge = challengeList.get(position);
        holder.content.setText(challenge.getOpportunity_content());
        holder.title.setText(challenge.getOpportunity_title());
        holder.creator.setText(challenge.getFirst_name()+" "+challenge.getLast_name());
        holder.time.setText(challenge.getPosted_time());
        holder.tv_prize.setText(challenge.getAward());
        holder.tv_deadline.setText(challenge.getChallenge_date()+", "+challenge.getChallenge_month()+" "+challenge.getChallenge_year());
        holder.tv_category.setText(challenge.getCategory());
        Picasso.with(this.context.getApplicationContext()).load(challenge.getUser_avatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.avatar);

        Picasso.with(this.context.getApplicationContext()).load(challenge.getOpportunity_attachment())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.image_post);
        holder.bn_participate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ParticipateChallengeActivity.class);
                intent.putExtra("challenge_item",challenge);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.challengeList.size();
    }
}
