package com.example.mwidadi.serikaliyangu.utils;

/**
 * Created by Mwidadi on 11/26/2017.
 */

public class APIConfig {
    public static final String PREPEND_LOCAL_URL = "http://serikaliyangu.or.tz/";
    //public static final String PREPEND_LOCAL_URL = "http://192.168.8.100:8000/";
    public static final String PREPEND_STORAGE_URL = PREPEND_LOCAL_URL+"storage/";
    public static final String USER_REGISTRATION_URL = PREPEND_LOCAL_URL+"api/create/user";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String USER_PROFILE_REFISTER_URL = PREPEND_LOCAL_URL+"api/create/profile";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_MIDDLE_NAME = "middle_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_DOB = "dob";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_MARITAL_STATUS = "marital_status";
    public static final String KEY_JOB_STATUS = "job_status";
    public static final String KEY_JOB_TITLE = "job_title";
    public static final String KEY_PROFILE_AVATAR = "avatar";
    public static final String KEY_RECIPIENT_ID = "recipient_id";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_COURSE_ID = "course_id";

    //FORUM REQUEST AND RESPONSE API
    public static final String CREATE_FORUM_URL = PREPEND_LOCAL_URL+"api/create/forum";
    public static final String GET_FORUM_URL = PREPEND_LOCAL_URL+"api/forums";
    public static final String KEY_TITLE = "title";
    public static final String KEY_POST = "post";
    public static final String KEY_ATTACHMENT = "attachment";
    public static final String COMMENT_FORUM_URL = PREPEND_LOCAL_URL+"api/create/forum/";
    public static final String GET_COMMENT_FORUM_URL = PREPEND_LOCAL_URL+"api/forum/";
    public static final String KEY_COMMENT = "comment";
    public static final String ENROLL_OPPORTUNITY_URL = PREPEND_LOCAL_URL+"api/create/opportunity/";
    public static final String GET_OPPORTUNITY_URL = PREPEND_LOCAL_URL+"api/opportunities";
    public static final String GET_ENROLLED_URL = PREPEND_LOCAL_URL+"api/opportunity/";
    public static final String KEY_DESCRIPTION = "description";

    //USER AUTH
    public static final String USER_LOGIN_URL = PREPEND_LOCAL_URL+"api/auth/login";
    public static final String ALL_REGION_URL = PREPEND_LOCAL_URL+"api/regions";
    public static final String ALL_DISTRICT_URL = PREPEND_LOCAL_URL+"api/regions/";
    public static final String KEY_DISTRICT_ID = "district_id";
    public static final String GET_TRAINING_URL = PREPEND_LOCAL_URL+"api/trainings";
    public static final String GET_TRAINING_COMMENT_URL = PREPEND_LOCAL_URL+"api/trainings/";
    public static final String CREATE_TRAINING_COMMENT_URL = PREPEND_LOCAL_URL+"api/create/training/";
    public static final String GET_USER_FORUM_URL = PREPEND_LOCAL_URL+"api/users/";
    public static final String DELETE_USER_FORUM_URL = PREPEND_LOCAL_URL+"api/delete/forum/";
    public static final String GET_BLOG_NEWS_URL = PREPEND_LOCAL_URL+"api/blog-posts";
    public static final String GET_BLOG_COMMENT_URL = PREPEND_LOCAL_URL+"api/blog-posts/";
    public static final String CREATE_BLOG_COMMENT_URL = PREPEND_LOCAL_URL+"api/create/blog-post/";
    public static final String EDIT_USER_PROFILE_URL = PREPEND_LOCAL_URL+"api/edit/profile/";
    public static final String GET_USER_ENROLLED_CHALLENGE = PREPEND_LOCAL_URL+"api/users/";
    public static final String GET_ALL_USERS = PREPEND_LOCAL_URL+"api/users";
    public static final String CREATE_NEW_THREAD = PREPEND_LOCAL_URL+"api/create/user/";
    public static final String GET_COURSE_URL = PREPEND_LOCAL_URL+"api/trainings/";
    public static final String ASSIGN_COURSE_URL = PREPEND_LOCAL_URL+"api/create/lesson";
    public static final String GET_COURSE_COMMENT_URL = PREPEND_LOCAL_URL+"api/courses/";
    public static final String ADD_COURSE_COMMENT_URL = PREPEND_LOCAL_URL+"api/create/lesson/comment";
    public static final String LEADERS_URL = PREPEND_LOCAL_URL+"api/leaders";
    public static final String GROUP_URL = PREPEND_LOCAL_URL+"api/groups";
    public static final String GROUP_CHAT_URL = PREPEND_LOCAL_URL+"api/create/group/";
    public static final String CREATE_THREAD_URL = PREPEND_LOCAL_URL+"api/create/user/";
    public static final String GET_ALL_T0HREAD_URL = PREPEND_LOCAL_URL+"api/threads";
    public static final String GET_MESSAGE_URL = PREPEND_LOCAL_URL+"api/threads/";
    public static final String GET_SORTED_THREAD_URL = PREPEND_LOCAL_URL+"api/threads/mwidadi/";

}
