package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class ForumHolder extends RecyclerView.ViewHolder {
    ImageView avatar;
    TextView content;
    TextView creator;
    TextView time;
    TextView title;
    CardView cvCard;

    public ForumHolder(View itemView) {
        super(itemView);
        avatar = (ImageView) itemView.findViewById(R.id.iv_image);
        content = (TextView) itemView.findViewById(R.id.tv_content);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        creator = (TextView) itemView.findViewById(R.id.tv_sender);
        time = (TextView) itemView.findViewById(R.id.tv_time);
        cvCard = (CardView) itemView.findViewById(R.id.cv_forum);
    }
}
