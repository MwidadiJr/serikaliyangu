package com.example.mwidadi.serikaliyangu.chat;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class UserHolder extends RecyclerView.ViewHolder {
    ImageView avatar;
    TextView name;
    TextView district;
    CardView cvCard;

    public UserHolder(View itemView) {
        super(itemView);
        avatar = (ImageView) itemView.findViewById(R.id.iv_image);
        name = (TextView) itemView.findViewById(R.id.tv_name);
        //district = (TextView) itemView.findViewById(R.id.tv_district);
        cvCard = (CardView) itemView.findViewById(R.id.cv_user);
    }
}
