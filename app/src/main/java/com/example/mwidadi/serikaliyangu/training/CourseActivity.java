package com.example.mwidadi.serikaliyangu.training;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import me.anwarshahriar.calligrapher.Calligrapher;

public class CourseActivity extends AppCompatActivity {

    private ArrayList<CourseModel> courseData = new ArrayList<>();
    private RecyclerView rv_course;
    private String link_url;
    private String video_id;
    private String youtube_image = "https://img.youtube.com/vi/";
    private CharSequence timePassedString;
    private String training_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.course_list_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change,R.anim.slide_down);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        initView();
        training_id = getIntent().getStringExtra("training_id");
        getCourse();
    }

    private void initView() {
        rv_course = (RecyclerView) findViewById(R.id.rv_course);
        rv_course.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rv_course.setHasFixedSize(true);
    }

    private void getCourse() {
        final StringRequest trainingRequest = new StringRequest(Request.Method.GET, APIConfig.GET_COURSE_URL+training_id, new Response.Listener<String>() {
            JSONObject trainingObject = null;

            @Override
            public void onResponse(String response) {
                try {
                    trainingObject = new JSONObject(response);
                    JSONObject singleTrainingObject = trainingObject.getJSONObject("training");
                    JSONArray courseArray = singleTrainingObject.getJSONArray("courses");
                    courseData.clear();
                    for (int j = 0; j < courseArray.length(); j++) {
                        JSONObject singleCourseObject = courseArray.getJSONObject(j);
                        CourseModel course = new CourseModel();
                        String course_id = singleCourseObject.getString("id");
                        String training_id = singleCourseObject.getString("training_id");
                        String title = singleCourseObject.getString("title");
                        String description = singleCourseObject.getString("description");
                        link_url = singleCourseObject.getString("link");
                        String created_at = singleCourseObject.getString("created_at");
                        String updated_at = singleCourseObject.getString("updated_at");
                        video_id = link_url.substring(link_url.lastIndexOf("?") + 3,link_url.lastIndexOf("&"));
                        String cover_image = youtube_image + video_id + "/0.jpg";

                        CharSequence formattedTime = formatTime(created_at);
                        String ago_time = formattedTime.toString();

                        course.setCourse_id(course_id);
                        course.setTraining_id(training_id);
                        course.setTitle(title);
                        course.setDescription(description);
                        course.setLink(link_url.substring(0,link_url.lastIndexOf("&")));
                        course.setCover_image(cover_image);
                        course.setCreated_at(ago_time);
                        course.setUpdated_at(updated_at);

                        courseData.add(course);
                    }

                    CourseAdapter adapter = new CourseAdapter(getApplicationContext(), courseData);
                    rv_course.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        trainingRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(trainingRequest);
    }

    private CharSequence formatTime(String dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void refresh() {
        if (this.courseData != null) {
            this.courseData.clear();
           getCourse();
            return;
        }
        else{
           getCourse();
        }
    }
}
