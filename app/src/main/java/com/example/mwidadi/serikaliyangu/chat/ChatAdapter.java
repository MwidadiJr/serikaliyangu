package com.example.mwidadi.serikaliyangu.chat;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.example.mwidadi.serikaliyangu.R;
import com.github.library.bubbleview.BubbleTextView;
import java.util.List;

/**
 * Created by Mwidadi on 12/15/2017.
 */

public class ChatAdapter extends BaseAdapter {
    Context mContext;
    List<Message> messageList;

    public ChatAdapter(Context mContext, List<Message> messageList) {
        this.mContext = mContext;
        this.messageList = messageList;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder holder;
        if (convertView == null){
            LayoutInflater messageInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = messageInflater.inflate(R.layout.list_receiverr, null);
            holder = new MessageViewHolder();
            holder.message_data = (BubbleTextView) convertView.findViewById(R.id.message_text_layout);
            convertView.setTag(holder);
        } else {
            holder = (MessageViewHolder) convertView.getTag();
        }

        Message message = (Message) getItem(position);

        holder.message_data.setText(message.message_content);
        return convertView;
    }

    public void add(Message message){
        messageList.add(message);
        notifyDataSetChanged();
    }
    private static class MessageViewHolder {
        public BubbleTextView message_data;
    }
}
