package com.example.mwidadi.serikaliyangu.fundCycle;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import com.example.mwidadi.serikaliyangu.R;
import me.anwarshahriar.calligrapher.Calligrapher;

public class FundCycleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_cycle);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.text_fundcycle_label);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                }
            });

        }
        getFundCycle();
    }

    private void getFundCycle(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("HOW TO GET FUND FROM YDF");
        builder.setMessage(R.string.dummy_text);
        builder.setCancelable(false);
        builder.setNegativeButton("GOT IT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"GOT IT",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                finish();
            }
        });

        builder.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"CANCELLED",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }
}
