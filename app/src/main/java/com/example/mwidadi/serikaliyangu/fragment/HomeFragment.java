package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.blog.BlogAdapter;
import com.example.mwidadi.serikaliyangu.blog.BlogAttachment;
import com.example.mwidadi.serikaliyangu.blog.BlogPost;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private RecyclerView rv_blog;
    private String user_id;
    private String news_id;
    private String news_title;
    private String news_post;
    private String news_posted;
    private String news_updated;
    private String user_phone;
    private String user_email;
    private String user_created;
    private String user_updated;
    private String profile_id;
    private String district_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String profile_avatar;
    private String dob;
    private String gender;
    private String marital_status;
    private String job_status;
    private String job_title;
    private String profile_created;
    private String profile_updated;
    private String district;
    private String attachment_id;
    private String attachment_blog_id;
    private String attachment;
    private String attachment_created;
    private String attachment_updated;
    private ProgressBar pBar;
    ArrayList<BlogPost> blogData = new ArrayList<>();
    ArrayList<BlogAttachment> attachmentData = new ArrayList<>();
    private BlogAdapter adapter;
    private String full_image_url = "";
    private String full_author_image_url = "";
    private CharSequence timePassedString;
    private String ago_time;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        rv_blog = (RecyclerView) v.findViewById(R.id.rv_blog);
        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        setHasOptionsMenu(true);
        rv_blog.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        getBlogNews();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()== R.id.action_refresh){
            if (pBar.getVisibility() == View.INVISIBLE){
                pBar.setVisibility(View.VISIBLE);
            }
            refresh();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getBlogNews(){
        pBar.setVisibility(View.VISIBLE);
        StringRequest newsRequest = new StringRequest(Request.Method.GET, APIConfig.GET_BLOG_NEWS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject blogObject = new JSONObject(response);
                    pBar.setVisibility(View.GONE);
                    JSONArray newsArray = blogObject.getJSONArray("post");
                    blogData.clear();
                    for (int i = 0;i<newsArray.length();i++){
                        JSONObject newsObject = newsArray.getJSONObject(i);
                        BlogPost blogPost = new BlogPost();
                        news_id = newsObject.getString("id");
                        user_id = newsObject.getString("user_id");
                        news_title = newsObject.getString("title");
                        news_post = newsObject.getString("post");
                        news_posted = newsObject.getString("created_at");
                        news_updated = newsObject.getString("updated_at");

                        CharSequence formatedTime = formatTime(news_posted);
                        ago_time = formatedTime.toString();

                        JSONObject userObject = newsObject.getJSONObject("user");
                        user_id = userObject.getString("id");
                        user_phone = userObject.getString("phone");
                        user_email = userObject.getString("email");
                        user_created = userObject.getString("created_at");
                        user_updated = userObject.getString("updated_at");

                        JSONObject profileObject = userObject.getJSONObject("profile");
                        profile_id = profileObject.getString("id");
                        user_id = profileObject.getString("user_id");
                        district_id = profileObject.getString("district_id");
                        first_name = profileObject.getString("first_name");
                        middle_name = profileObject.getString("middle_name");
                        last_name = profileObject.getString("last_name");
                        profile_avatar = profileObject.getString("avatar");
                        dob = profileObject.getString("dob");
                        gender = profileObject.getString("gender");
                        marital_status = profileObject.getString("marital_status");
                        job_status = profileObject.getString("job_status");
                        job_title = profileObject.getString("job_title");
                        profile_created = profileObject.getString("created_at");
                        profile_updated  = profileObject.getString("updated_at");
                        district = profileObject.getString("district");

                        full_author_image_url = APIConfig.PREPEND_STORAGE_URL+profile_avatar;

                        JSONArray attachmentArray = newsObject.getJSONArray("attachments");
                        for ( int j = 0;j<attachmentArray.length();j++){
                            JSONObject attachmentObject = attachmentArray.getJSONObject(j);
                            BlogAttachment blogAttachment = new BlogAttachment();
                            attachment_id = attachmentObject.getString("id");
                            attachment_blog_id = attachmentObject.getString("blog_id");
                            attachment = attachmentObject.getString("attachment");
                            attachment_created = attachmentObject.getString("created_at");
                            attachment_updated = attachmentObject.getString("updated_at");

                            full_image_url = APIConfig.PREPEND_STORAGE_URL+attachment;

                            blogAttachment.setAttachment_id(attachment_id);
                            blogAttachment.setAttachment(full_image_url);
                            blogAttachment.setBlog_id(attachment_blog_id);
                            blogAttachment.setCreated_at(attachment_created);
                            blogAttachment.setUpdated_at(attachment_created);
                            blogPost.setAttachment(full_image_url);

                            attachmentData.add(blogAttachment);
                        }

                         blogPost.setPost_id(news_id);
                         blogPost.setUser_id(user_id);
                         blogPost.setPost_title(news_title);
                         blogPost.setPost_content(news_post);
                         blogPost.setPost_created(ago_time);
                         blogPost.setPost_updated(news_updated);
                         blogPost.setUser_email(user_email);
                         blogPost.setUser_phone(user_phone);
                         blogPost.setFirst_name(first_name);
                         blogPost.setMiddle_name(middle_name);
                         blogPost.setLast_name(last_name);
                         blogPost.setUser_avatar(full_author_image_url);
                         blogPost.setDistrict_id(district_id);
                         blogPost.setUser_district(district);

                         blogData.add(blogPost);
                    }

                    adapter = new BlogAdapter(getContext(), blogData);
                    rv_blog.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                Snackbar.make(rv_blog, R.string.no_internet, Snackbar.LENGTH_INDEFINITE).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getBlogNews();
                    }
                }).show();
                pBar.setVisibility(View.INVISIBLE);
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        newsRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(newsRequest);
    }

    private CharSequence formatTime(String dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }

    public void refresh() {
        if (this.blogData != null) {
            this.blogData.clear();
            getBlogNews();
            return;
        }
        else{
            getBlogNews();
        }
    }

}
