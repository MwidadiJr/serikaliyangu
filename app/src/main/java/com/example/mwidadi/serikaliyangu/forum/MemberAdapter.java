package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class MemberAdapter extends RecyclerView.Adapter<MemberHolder> {
    Context context;
    ArrayList<MemberModel> memberList = new ArrayList<>();

    public MemberAdapter(Context context, ArrayList<MemberModel> memberList){
        this.memberList = memberList;
        this.context = context;
    }

    @Override
    public MemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_user,parent,false);
        MemberHolder member = new MemberHolder(v);
        return member;
    }

    @Override
    public void onBindViewHolder(MemberHolder holder, int position) {
        final MemberModel member = memberList.get(position);
        holder.name.setText(member.getFirst_name()+" "+member.getMiddle_name()+" "+member.getLast_name());
        holder.phone.setText(member.getPhone());
        holder.email.setText(member.getEmail());
        holder.gender.setText(member.getGender());
        Picasso.with(this.context.getApplicationContext()).load(member.getAvatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        return this.memberList.size();
    }
}
