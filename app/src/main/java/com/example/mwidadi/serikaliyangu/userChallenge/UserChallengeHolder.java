package com.example.mwidadi.serikaliyangu.userChallenge;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class UserChallengeHolder extends RecyclerView.ViewHolder {
    TextView content;
    TextView time;
    TextView title;
    CardView cvCard;

    public UserChallengeHolder(View itemView) {
        super(itemView);
        content = (TextView) itemView.findViewById(R.id.tv_content);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        time = (TextView) itemView.findViewById(R.id.tv_time);
        cvCard = (CardView) itemView.findViewById(R.id.cv_forum);
    }
}
