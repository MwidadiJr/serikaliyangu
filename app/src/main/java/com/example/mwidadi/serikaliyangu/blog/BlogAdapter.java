package com.example.mwidadi.serikaliyangu.blog;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class BlogAdapter extends RecyclerView.Adapter<BlogHolder> {
    Context context;
    ArrayList<BlogPost> newsList = new ArrayList<>();

    public BlogAdapter(Context context, ArrayList<BlogPost> newsList){
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public BlogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_blog,parent,false);
        BlogHolder blogHolder = new BlogHolder(v);
        return blogHolder;
    }

    @Override
    public void onBindViewHolder(BlogHolder holder, int position) {
       final BlogPost news = newsList.get(position);
        holder.tv_title.setText(news.getPost_title());
        holder.tv_author.setText(news.getFirst_name());
        holder.tv_time.setText(news.getPost_created());
        Picasso.with(this.context.getApplicationContext()).load(news.getAttachment())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.kb_view);
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), BlogDetailActivity.class);
                intent.putExtra("news_item",news);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.newsList.size();
    }
}
