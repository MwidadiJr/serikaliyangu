package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class ForumAdapter extends RecyclerView.Adapter<ForumHolder> {
    Context context;
    ArrayList<Forum> forumList = new ArrayList<>();

    public ForumAdapter(Context context, ArrayList<Forum> forumList){
        this.forumList = forumList;
        this.context = context;
    }

    @Override
    public ForumHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_forum,parent,false);
        ForumHolder newsHolder = new ForumHolder(v);
        return newsHolder;
    }

    @Override
    public void onBindViewHolder(ForumHolder holder, int position) {
       final Forum forum = forumList.get(position);
        holder.content.setText(forum.getForum_content());
        holder.title.setText(forum.getForum_title());
        holder.creator.setText(forum.getFirst_name()+" "+forum.getLast_name());
        holder.time.setText(forum.getPosted_time());
        Picasso.with(this.context.getApplicationContext()).load(forum.getUser_avatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.avatar);
        holder.cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ForumDetailActivity.class);
                intent.putExtra("forum_item",forum);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.forumList.size();
    }
}
