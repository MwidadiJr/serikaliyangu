package com.example.mwidadi.serikaliyangu.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mwidadi on 12/6/2017.
 */

public class LanguagePreference {
    public static final String KISWAHILI_SET = "KISWAHILI";
    public static final String ENGLISH_SET = "ENGLISH";
    private static final String PREF_NAME = "LanguagePref";
    private static final String KEY_LANGUAGE = "language";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;
    public LanguagePreference(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();

    }

    public void setLanguage(String language){
        editor.putString(KEY_LANGUAGE,language);
        editor.commit();
    }
    public String getLanguage() {
        return preferences.getString(KEY_LANGUAGE, "en");
    }

}
