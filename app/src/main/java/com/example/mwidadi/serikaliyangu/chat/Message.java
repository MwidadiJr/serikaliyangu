package com.example.mwidadi.serikaliyangu.chat;

/**
 * Created by Mwidadi on 10/22/2017.
 */

public class Message {
   public String thread_id;
   private String user_id;
   private String recepient_id;
   public String message_content;
   private String receiver_first_name;
   private String receiver_middle_name;
   private String receiver_last_name;
   private String receiver_avatar;
   private String created_at;

}
