package com.example.mwidadi.serikaliyangu.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.mwidadi.serikaliyangu.AboutSerikaliYangu;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.userChallenge.UserChallengeActivity;
import com.example.mwidadi.serikaliyangu.userForum.ForumManagementActivity;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.LanguagePreference;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.StringFormatter;
import com.squareup.picasso.Picasso;
import java.util.Locale;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageView iv_profile;
    private TextView tv_name,tv_description,tv_location;
    private Button bn_edit_profile,bn_post;
    private LinearLayout ll_setting,ll_share,ll_policy,ll_about,ll_logout;
    private PreferenceManager preferenceManager;
    private String first_name;
    private String last_name;
    private String middle_name;
    private String phone_number;
    private String email;
    private String gender;
    private String job_status;
    private String job_title;
    private String avatar;
    private String birth_day;
    private String user_id;
    private String full_avatar_url = "";
    private String share_text = "At vero eos et accusamus et iusto odio molestias excepturi";
    private StringFormatter stringFormatter;
    private String formatted_job_title;
    private String region_name,district_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.text_profile_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });
        }
        preferenceManager = new PreferenceManager(this);
        stringFormatter = new StringFormatter(this);
        getUserInfo();
        initView();
        setInfo();
    }

    private void initView(){
        iv_profile = (ImageView) findViewById(R.id.iv_change_image);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_location = (TextView) findViewById(R.id.tv_location);
        bn_edit_profile = (Button) findViewById(R.id.bn_edit_profile);
        bn_post = (Button) findViewById(R.id.bn_post);
        ll_setting = (LinearLayout) findViewById(R.id.ll_setting);
        ll_about = (LinearLayout) findViewById(R.id.ll_about);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        ll_policy = (LinearLayout) findViewById(R.id.ll_policy);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        iv_profile.setOnClickListener(this);
        bn_edit_profile.setOnClickListener(this);
        bn_post.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        ll_policy.setOnClickListener(this);
        ll_share.setOnClickListener(this);
        ll_about.setOnClickListener(this);
        ll_setting.setOnClickListener(this);
    }

    private void setInfo(){
        Picasso.with(this.getApplicationContext()).load(full_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(iv_profile);
        tv_name.setText(first_name+" "+middle_name+" "+last_name);
        tv_description.setText(formatted_job_title);
        tv_location.setText(region_name);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_change_image:
                Intent profileIntent = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(profileIntent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                break;

            case R.id.bn_edit_profile:
                Intent intent = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                break;

            case R.id.bn_post:
                showOption();
                break;

            case R.id.ll_setting:
                showSetting();
                break;

            case R.id.ll_about:
                Intent aboutIntent = new Intent(getApplicationContext(), AboutSerikaliYangu.class);
                startActivity(aboutIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case R.id.ll_logout:
                preferenceManager.clearSession();
                Intent mainIntent = new Intent(ProfileActivity.this,LoginActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);
                finish();
                break;

            case R.id.ll_policy:
                terms();
                break;

            case R.id.ll_share:
               share();
                break;

            default:
                Toast.makeText(getApplicationContext(),"DEFAULT",Toast.LENGTH_SHORT).show();
        }
    }

    private void getUserInfo(){
        first_name = preferenceManager.getFirstName();
        middle_name = preferenceManager.getMiddleName();
        last_name = preferenceManager.getLastName();
        phone_number = preferenceManager.getPhone();
        email = preferenceManager.getEmail();
        gender = preferenceManager.getGender();
        birth_day = preferenceManager.getBirthDay();
        job_title = preferenceManager.getJobTitle();
        job_status = preferenceManager.getJobStatus();
        avatar = preferenceManager.getAvatar();
        user_id = preferenceManager.getUserId();
        full_avatar_url = APIConfig.PREPEND_STORAGE_URL+avatar;
        formatted_job_title = stringFormatter.properCase(job_title);
        region_name = stringFormatter.properCase(preferenceManager.getRegionName());
        district_name = stringFormatter.properCase(preferenceManager.getDistrictName());
    }

    private void showOption() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.text_my_posts));
        builder.setMessage(getString(R.string.text_my_post_info));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_forum_capital, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                Intent intent = new Intent(getApplicationContext(), ForumManagementActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.text_challenge_capital, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), UserChallengeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void share() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,share_text);
        startActivity(Intent.createChooser(shareIntent, "Serikali Yangu share options"));
    }

    private void terms(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("TERMS AND POLICY");
        builder.setMessage(R.string.dummy_text);
        builder.setCancelable(false);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void showSetting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.text_chnage_language));
        builder.setMessage(getString(R.string.change_language_setting));
        builder.setIcon(R.drawable.ic_action_language);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_english, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                setCurrentLanguage("en");
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.text_swahili, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                setCurrentLanguage("sw");
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void setCurrentLanguage(String currentLanguage) {
        LanguagePreference languagePref = new LanguagePreference(getApplicationContext());
        languagePref.setLanguage(currentLanguage);
        Locale locale = new Locale(languagePref.getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        recreate();
    }

}
