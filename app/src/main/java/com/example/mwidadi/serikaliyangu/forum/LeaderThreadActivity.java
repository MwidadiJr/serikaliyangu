package com.example.mwidadi.serikaliyangu.forum;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class LeaderThreadActivity extends AppCompatActivity implements View.OnClickListener {

    private PreferenceManager preferenceManager;
    private String user_id;
    private EditText et_comment;
    private RecyclerView rv_thread;
    private ImageButton iv_send;
    private CircleImageView chat_add_btn;
    private String received_first_name;
    private String received_middle_name;
    private String received_last_name;
    private String received_user_id;
    private String received_user_avatar;
    private String full_user_avatar_url = "";
    private String user_avatar = "";
    private LeaderModel leader;
    private String leader_id;
    private ArrayList<ThreadModel> threadData = new ArrayList<>();
    private String full_avatar_url = "";
    private CharSequence timePassedString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_thread);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.thread_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change, R.anim.slide_down);
                }
            });
        }
        preferenceManager = new PreferenceManager(getApplicationContext());
        user_id = preferenceManager.getUserId();
        received_first_name = preferenceManager.getFirstName();
        received_middle_name = preferenceManager.getMiddleName();
        received_last_name = preferenceManager.getLastName();
        received_user_id = preferenceManager.getUserId();
        received_user_avatar = preferenceManager.getAvatar();
        getInfo();
        full_user_avatar_url = APIConfig.PREPEND_STORAGE_URL + received_user_avatar;
        initView();
        getThread();
    }

    private void initView() {
        et_comment = (EditText) findViewById(R.id.et_comment);
        iv_send = (ImageButton) findViewById(R.id.iv_send);
        rv_thread = (RecyclerView) findViewById(R.id.rv_thread);
        chat_add_btn = (CircleImageView) findViewById(R.id.chat_add_btn);
        Picasso.with(getApplicationContext()).load(full_user_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(chat_add_btn);
        rv_thread.setLayoutManager(new LinearLayoutManager(this));
        rv_thread.setHasFixedSize(true);
        iv_send.setOnClickListener(this);
    }

    private void getInfo() {
        leader = (LeaderModel) getIntent().getSerializableExtra("leader_item");
        leader_id = leader.getUser_id();
    }

    private void createThread(final String recepient_id, final String message) {
        StringRequest threadRequest = new StringRequest(Request.Method.POST, APIConfig.CREATE_THREAD_URL+user_id+"/thread", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("Message Sent!")){
                    showSent();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Failed to send message, please try again later!",Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_RECIPIENT_ID, recepient_id);
                params.put(APIConfig.KEY_MESSAGE, message);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        threadRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(threadRequest);
    }

    private void getThread() {
        final StringRequest getThreadRequest = new StringRequest(Request.Method.GET, APIConfig.GET_SORTED_THREAD_URL+received_user_id+"/"+leader_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject threadObject = new JSONObject(response);
                    JSONArray threadArray = threadObject.getJSONArray("threads");
                    for (int i = 0; i < threadArray.length(); i++) {
                        JSONObject singleThreadObject = threadArray.getJSONObject(i);
                        ThreadModel threadModel = new ThreadModel();
                        String thread_id = singleThreadObject.getString("id");
                        String sender_id = singleThreadObject.getString("user_id");
                        String recipient_id = singleThreadObject.getString("recipient_id");
                        String created_at = singleThreadObject.getString("created_at");
                        String message = singleThreadObject.getString("message");

                        CharSequence formattedTime = formatTime(created_at);
                        String ago_time = formattedTime.toString();

                        JSONObject userObject = singleThreadObject.getJSONObject("user");
                        JSONObject profileObject = userObject.getJSONObject("profile");
                        String profile_id = profileObject.getString("id");
                        String first_name = profileObject.getString("first_name");
                        String middle_name = profileObject.getString("middle_name");
                        String last_name = profileObject.getString("last_name");
                        String avatar = profileObject.getString("avatar");

                        user_avatar = APIConfig.PREPEND_STORAGE_URL + avatar;

                        threadModel.setThread_id(thread_id);
                        threadModel.setSender_id(sender_id);
                        threadModel.setRecipient_id(recipient_id);
                        threadModel.setCreated_at(ago_time);
                        threadModel.setMessage(message);
                        threadModel.setProfile_id(profile_id);
                        threadModel.setFirst_name(first_name);
                        threadModel.setMiddle_name(middle_name);
                        threadModel.setLast_name(last_name);
                        threadModel.setAvatar(user_avatar);

                        threadData.add(threadModel);
                    }

                    ThreadAdapter adapter = new ThreadAdapter(getApplicationContext(), threadData);
                    rv_thread.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getThreadRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(getThreadRequest);
    }

    private CharSequence formatTime(String dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }

    @Override
    public void onClick(View v) {
        if (v == iv_send) {
            createThread(leader_id, et_comment.getText().toString().trim());
        }
    }

    private void showSent() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.text_successfully_sent));
        builder.setMessage(getString(R.string.messasge_successfully_sent_label));
        builder.setIcon(R.drawable.ic_action_ok);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
              finish();
            }
        });
        builder.show();
    }
}
