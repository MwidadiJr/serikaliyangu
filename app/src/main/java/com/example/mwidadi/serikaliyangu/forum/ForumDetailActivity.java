package com.example.mwidadi.serikaliyangu.forum;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.profile.LoginActivity;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import me.biubiubiu.justifytext.library.JustifyTextView;

public class ForumDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageView iv_avatar,iv_add_comment,iv_share,iv_read_comment;
    private TextView tv_user_name,tv_posted,tv_count,tv_title,tv_add_comment,tv_read_comment;
    private JustifyTextView tv_description;
    private Forum forum;
    private String user_avatar;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String posted_time;
    private String forum_title;
    private String forum_content;
    private LinearLayout ll_detail;
    private String forum_id;
    private PreferenceManager preferenceManager;
    private String total_comment;
    private RecyclerView rv_comment;
    private String full_image_path = "";
    ArrayList<ForumComment> commentData = new ArrayList<>();
    private CommentAdapter adapter;
    private View clProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.forum_detail_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                }
            });
        }
        initView();
        preferenceManager = new PreferenceManager(this);
        forum = (Forum)getIntent().getSerializableExtra("forum_item");
        user_avatar = forum.getUser_avatar();
        first_name = forum.getFirst_name();
        middle_name = forum.getMiddle_name();
        last_name = forum.getLast_name();
        posted_time = forum.getPosted_time();
        forum_title = forum.getForum_title();
        forum_content = forum.getForum_content();
        forum_id = forum.getForum_id();
        getForumComment(forum_id);
        setToViews();
    }

    private void initView(){
        iv_avatar = (ImageView) findViewById(R.id.iv_avatar);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        tv_posted = (TextView) findViewById(R.id.tv_posted);
        tv_description = (JustifyTextView) findViewById(R.id.tv_description);
        tv_count = (TextView) findViewById(R.id.tv_count);
        tv_title = (TextView) findViewById(R.id.tv_title);
        clProgress = findViewById(R.id.cl);
        iv_add_comment = (ImageView) findViewById(R.id.iv_add_comment);
        tv_add_comment = (TextView) findViewById(R.id.tv_add_comment);
        ll_detail = (LinearLayout) findViewById(R.id.ll_detail);
        iv_share = (ImageView) findViewById(R.id.iv_share);
        iv_read_comment = (ImageView) findViewById(R.id.iv_read_comment);
        tv_read_comment = (TextView) findViewById(R.id.tv_read_comment);
        rv_comment = (RecyclerView) findViewById(R.id.rv_comment);
        rv_comment = (RecyclerView) findViewById(R.id.rv_comment);
        rv_comment.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        iv_add_comment.setOnClickListener(this);
        tv_add_comment.setOnClickListener(this);
        iv_read_comment.setOnClickListener(this);
        tv_read_comment.setOnClickListener(this);
        iv_share.setOnClickListener(this);
    }

    private void setToViews(){
        tv_user_name.setText(first_name+" "+middle_name+" "+last_name);
        tv_posted.setText(posted_time);
        tv_description.setText(forum_content);
        tv_title.setText(forum_title);
        Picasso.with(getApplicationContext()).load(user_avatar).placeholder(R.drawable.ic_action_avatar).into(iv_avatar);
    }

    private void share(){
        Bitmap bitmap = getBitmapFromView(ll_detail);
        try {
            File file = new File(getCacheDir(), "serikaliyangu" + ".png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share post via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null) {
            bgDrawable.draw(canvas);
        }   else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                case R.id.iv_add_comment:
                    rv_comment.setVisibility(View.GONE);
                    Intent intent = new Intent(getApplicationContext(), ForumCommentActivity.class);
                    intent.putExtra("forum_id",forum_id);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                    break;

                case R.id.tv_add_comment:
                    rv_comment.setVisibility(View.GONE);
                    Intent commentIntent = new Intent(getApplicationContext(), ForumCommentActivity.class);
                    commentIntent.putExtra("forum_id",forum_id);
                    startActivity(commentIntent);
                    overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                    break;

                case R.id.tv_read_comment:
                    rv_comment.setVisibility(View.VISIBLE);
                    break;

                case R.id.iv_read_comment:
                    rv_comment.setVisibility(View.VISIBLE);
                    break;

                case R.id.iv_share:
                    share();
                    break;

                default:

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout){
            preferenceManager.clearSession();
            Intent mainIntent = new Intent(ForumDetailActivity.this,LoginActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getForumComment(final String forum_id){
        clProgress.setVisibility(View.VISIBLE);
        final StringRequest commentRequest = new StringRequest(Request.Method.GET, APIConfig.GET_COMMENT_FORUM_URL+forum_id+"/comments", new Response.Listener<String>() {
            JSONObject commentObject = null;
            @Override
            public void onResponse(String response) {
                clProgress.setVisibility(View.GONE);
                try {
                    commentObject = new JSONObject(response);
                    JSONObject comments = commentObject.getJSONObject("comments");
                    JSONArray commentArray = comments.getJSONArray("comments");
                    total_comment = String.valueOf(commentArray.length());
                    tv_count.setText("("+total_comment+")");
                    for (int i = 0;i<commentArray.length();i++){
                        JSONObject allCommentObject = commentArray.getJSONObject(i);
                        ForumComment forumComment = new ForumComment();
                        String comment = allCommentObject.getString("comment");
                        String created_at = allCommentObject.getString("created_at");
                        JSONObject userObject = allCommentObject.optJSONObject("user");
                        JSONObject profileObject = userObject.getJSONObject("profile");
                        String first_name = profileObject.getString("first_name");
                        String middle_name = profileObject.getString("middle_name");
                        String last_name = profileObject.getString("last_name");
                        String user_avatar = profileObject.getString("avatar");
                        full_image_path = APIConfig.PREPEND_STORAGE_URL + user_avatar;

                        forumComment.setComment_content(comment);
                        forumComment.setComment_posted(created_at);
                        forumComment.setFirst_name(first_name);
                        forumComment.setMiddle_name(middle_name);
                        forumComment.setLast_name(last_name);
                        forumComment.setUser_avatar(full_image_path);
                        commentData.add(forumComment);
                    }

                    adapter = new CommentAdapter(getApplicationContext(),commentData);
                    adapter.notifyDataSetChanged();
                    rv_comment.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                clProgress.setVisibility(View.GONE);
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(commentRequest);
    }
}
