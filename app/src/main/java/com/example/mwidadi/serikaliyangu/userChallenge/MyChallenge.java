package com.example.mwidadi.serikaliyangu.userChallenge;

/**
 * Created by Mwidadi on 12/7/2017.
 */

public class MyChallenge {
    private String enrolled_id;
    private String user_id;
    private String opportunity_id;
    private String description;
    private String created_at;
    private String updated_at;

    public String getEnrolled_id() {
        return enrolled_id;
    }

    public void setEnrolled_id(String enrolled_id) {
        this.enrolled_id = enrolled_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOpportunity_id() {
        return opportunity_id;
    }

    public void setOpportunity_id(String opportunity_id) {
        this.opportunity_id = opportunity_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
