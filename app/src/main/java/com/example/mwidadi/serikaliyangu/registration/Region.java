package com.example.mwidadi.serikaliyangu.registration;

/**
 * Created by Mwidadi on 11/30/2017.
 */

public class Region {
    private String region_id;
    private String region_name;
    private String region_createc_at;
    private String region_updated_at;

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getRegion_createc_at() {
        return region_createc_at;
    }

    public void setRegion_createc_at(String region_createc_at) {
        this.region_createc_at = region_createc_at;
    }

    public String getRegion_updated_at() {
        return region_updated_at;
    }

    public void setRegion_updated_at(String region_updated_at) {
        this.region_updated_at = region_updated_at;
    }
}
