package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.SquareImageView;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class LeaderHolder extends RecyclerView.ViewHolder {
    SquareImageView avatar;
    TextView name;
    TextView title;
    TextView location;
    CardView cvCard;

    public LeaderHolder(View itemView) {
        super(itemView);
        avatar = (SquareImageView) itemView.findViewById(R.id.iv_avatar);
        name = (TextView) itemView.findViewById(R.id.tv_name);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        location = (TextView) itemView.findViewById(R.id.tv_location);
        cvCard = (CardView) itemView.findViewById(R.id.cv_leader);
    }
}
