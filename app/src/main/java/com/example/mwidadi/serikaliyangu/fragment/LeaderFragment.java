package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.forum.LeaderAdapter;
import com.example.mwidadi.serikaliyangu.forum.LeaderModel;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaderFragment extends Fragment {

    private ArrayList<LeaderModel> leaderData = new ArrayList<>();
    private RecyclerView rv_leader;
    private String full_image_path = "";
    private ProgressBar pBar;
    private PreferenceManager preferenceManager;
    private String user_district_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_leader, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        rv_leader = (RecyclerView) v.findViewById(R.id.rv_leader);
        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        rv_leader.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_leader.setHasFixedSize(true);
        preferenceManager = new PreferenceManager(getContext());
        user_district_id = preferenceManager.getDistrictId();
        getLeaders();
        return v;
    }

    private void getLeaders(){
        pBar.setVisibility(View.VISIBLE);
        final StringRequest leaderRequest = new StringRequest(Request.Method.GET, APIConfig.LEADERS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject leaderObject = new JSONObject(response);
                    pBar.setVisibility(View.GONE);
                    JSONArray leadersArray = leaderObject.getJSONArray("leaders");
                    leaderData.clear();
                    for (int i = 0;i<leadersArray.length();i++){
                        JSONObject singleLeaderObject = leadersArray.getJSONObject(i);
                        LeaderModel leader = new LeaderModel();
                        String leader_id = singleLeaderObject.getString("id");
                        String user_id = singleLeaderObject.getString("user_id");
                        JSONObject userObject = singleLeaderObject.getJSONObject("user");
                        JSONObject profileObject = userObject.getJSONObject("profile");
                        String profile_id = profileObject.getString("id");
                        String district_id = profileObject.getString("district_id");
                        String first_name = profileObject.getString("first_name");
                        String middle_name = profileObject.getString("middle_name");
                        String last_name = profileObject.getString("last_name");
                        String avatar = profileObject.getString("avatar");
                        String dob = profileObject.getString("dob");
                        String gender = profileObject.getString("gender");
                        String marital_status = profileObject.getString("marital_status");
                        String job_status = profileObject.getString("job_status");
                        String job_title = profileObject.getString("job_title");

                        full_image_path = APIConfig.PREPEND_STORAGE_URL + avatar;

                        JSONObject districtObject = profileObject.getJSONObject("district");
                        String district_name = districtObject.getString("name");

                        JSONObject regionObject = districtObject.getJSONObject("region");
                        String region_id = regionObject.getString("id");
                        String region_name = regionObject.getString("name");

                        String proper_region = properCase(region_name);
                        String proper_district = properCase(district_name);

                        leader.setLeader_id(leader_id);
                        leader.setUser_id(user_id);
                        leader.setProfile_id(profile_id);
                        leader.setDistrict_id(district_id);
                        leader.setFirst_nsme(first_name);
                        leader.setMiddle_name(middle_name);
                        leader.setLast_name(last_name);
                        leader.setAvatar(full_image_path);
                        leader.setDob(dob);
                        leader.setGender(gender);
                        leader.setMarital_status(marital_status);
                        leader.setJob_status(job_status);
                        leader.setJob_title(job_title);
                        leader.setDistrict_name(proper_district);
                        leader.setRegion_id(region_id);
                        leader.setRegion_name(proper_region);

                        leaderData.add(leader);

//                        if (user_district_id.equalsIgnoreCase(district_id)){
//                            leaderData.add(leader);
//                        }
//                        else {
//                            Toast.makeText(getContext(),"There is no leader registered in your location yet!",Toast.LENGTH_LONG).show();
//                        }


                    }

                    LeaderAdapter adapter = new LeaderAdapter(getContext(),leaderData);
                    rv_leader.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getActivity(). getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        leaderRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(leaderRequest);
    }

    public String properCase(String paramString) {
        if (paramString.length() == 0) {
            return "";
        }
        if (paramString.length() == 1) {
            return paramString.toUpperCase();
        }
        return paramString.substring(0, 1).toUpperCase() + paramString.substring(1).toLowerCase();
    }

}
