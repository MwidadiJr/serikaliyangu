package com.example.mwidadi.serikaliyangu.blog;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;
import com.flaviofaria.kenburnsview.KenBurnsView;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class BlogHolder extends RecyclerView.ViewHolder {
    KenBurnsView kb_view;
    TextView tv_author;
    TextView tv_title;
    CardView card_view;
    TextView tv_time;

    public BlogHolder(View itemView) {
        super(itemView);
        kb_view = (KenBurnsView) itemView.findViewById(R.id.iv_image);
        tv_author = (TextView) itemView.findViewById(R.id.tv_author);
        tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        card_view = (CardView) itemView.findViewById(R.id.card_view);
        tv_time = (TextView) itemView.findViewById(R.id.tv_time);
    }
}
