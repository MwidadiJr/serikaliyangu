package com.example.mwidadi.serikaliyangu.registration;

/**
 * Created by Mwidadi on 11/30/2017.
 */

public class District {
    private String district_id;
    private String region_id;
    private String district_name;
    private String district_created_at;
    private String district_updated_at;

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getDistrict_created_at() {
        return district_created_at;
    }

    public void setDistrict_created_at(String district_created_at) {
        this.district_created_at = district_created_at;
    }

    public String getDistrict_updated_at() {
        return district_updated_at;
    }

    public void setDistrict_updated_at(String district_updated_at) {
        this.district_updated_at = district_updated_at;
    }
}
