package com.example.mwidadi.serikaliyangu.training;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.forum.ForumComment;
import com.squareup.picasso.Picasso;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class TrainingCommentAdapter extends RecyclerView.Adapter<TrainingCommentHolder> {
    Context context;
    ArrayList<TrainingComment> commentList = new ArrayList<>();
    private CharSequence timePassedString;
    private String user_id;

    public TrainingCommentAdapter(Context context, ArrayList<TrainingComment> commentList){
        this.commentList = commentList;
        this.context = context;
    }

    @Override
    public TrainingCommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_comment,parent,false);
        TrainingCommentHolder commentHolder = new TrainingCommentHolder(v);
        return commentHolder;
    }

    @Override
    public void onBindViewHolder(TrainingCommentHolder holder, int position) {
       final TrainingComment commentModel = commentList.get(position);
        holder.user_first_name.setText(commentModel.getFirst_name()+" "+commentModel.getLast_name());
        holder.commnet.setText(commentModel.getComment_content());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;

        try {
            if (commentList.get(position).getCreated_at() == null || commentList.get(position).getCreated_at() == ""){
                date = Calendar.getInstance().getTime();
                holder.user_first_name.setText(commentModel.getFirst_name()+ " " + commentModel.getLast_name());
            }
            else{
                date = df.parse(commentList.get(position).getCreated_at());
                holder.time.setText(timePassedString);
            }
        }catch (ParseException e) {
             date = Calendar.getInstance().getTime();
        }

        holder.time.setText(DateUtils.getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        Picasso.with(this.context.getApplicationContext()).load(commentModel.getUser_avatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.userImage);

        holder.cvCard.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context,commentModel.getFirst_name(),Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }

    public interface OnActionOnComment{
        void onDelete(ForumComment comment);
        void onDoEdit(ForumComment comment);
    }

    public int getItemViewType(int paramInt) {
        return paramInt;
    }

    public void removeItem(ForumComment comment){
        commentList.remove(comment);
        notifyDataSetChanged();
    }

    public Object getItem(int paramInt) {
        return this.commentList.get(paramInt);
    }

    public long getItemId(int paramInt) {
        return paramInt;
    }

    @Override
    public int getItemCount() {
        return this.commentList.size();
    }
}
