package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class GroupChatActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText et_comment;
    private RecyclerView rv_comment;
    private ImageButton iv_send;
    private CircleImageView chat_add_btn;
    private PreferenceManager preferenceManager;
    private String received_first_name;
    private String received_middle_name;
    private String received_last_name;
    private String received_user_id;
    private String received_user_avatar;
    private String full_user_avatar_url = "";
    private String user_avatar = "";
    private GroupModel group;
    private String group_id;
    private String group_name;
    private String group_description;
    private GroupChatModel chat;
    private ArrayList<GroupChatModel> chatData = new ArrayList<>();
    private GroupChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change,R.anim.slide_down);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        preferenceManager = new PreferenceManager(this);
        received_first_name = preferenceManager.getFirstName();
        received_middle_name = preferenceManager.getMiddleName();
        received_last_name = preferenceManager.getLastName();
        received_user_id = preferenceManager.getUserId();
        received_user_avatar = preferenceManager.getAvatar();
        full_user_avatar_url = APIConfig.PREPEND_STORAGE_URL+received_user_avatar;
        initView();
        getData();
        getGroupInformation();
        getChat();
    }

    private void initView(){
        et_comment = (EditText) findViewById(R.id.et_comment);
        iv_send = (ImageButton) findViewById(R.id.iv_send);
        rv_comment = (RecyclerView) findViewById(R.id.rv_comment);
        chat_add_btn = (CircleImageView) findViewById(R.id.chat_add_btn);
        Picasso.with(getApplicationContext()).load(full_user_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(chat_add_btn);
        rv_comment.setLayoutManager(new LinearLayoutManager(this));
        iv_send.setOnClickListener(this);
    }

    private void getData(){
        group = (GroupModel) getIntent().getSerializableExtra("group_item");
        group_id = group.getGroup_id();
        group_name = group.getTitle();
        group_description = group.getDescription();
        getSupportActionBar().setTitle(group_name);
    }

    private void addChat(final String user_id,final String comment){
        StringRequest chatRequest = new StringRequest(Request.Method.POST, APIConfig.GROUP_CHAT_URL+group_id+"/comment", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_COMMENT,comment);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        chatRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(chatRequest);
    }

    private void getGroupInformation(){
        StringRequest groupRequest = new StringRequest(Request.Method.GET, APIConfig.GROUP_URL+"/"+group_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject mainGroupObject = new JSONObject(response);
                    JSONObject groupObject = mainGroupObject.getJSONObject("group");
                    String group_id = groupObject.getString("id");
                    String user_id = groupObject.getString("user_id");
                    String district_id = groupObject.getString("district_id");
                    String group_title = groupObject.getString("title");
                    String group_description = groupObject.getString("description");

                    JSONObject userObject = groupObject.getJSONObject("user");
                    String admin_phone = userObject.getString("phone");
                    String admin_email = userObject.getString("email");

                    JSONObject profileObject = userObject.getJSONObject("profile");
                    String profile_id = profileObject.getString("id");
                    String admin_first_name = profileObject.getString("first_name");
                    String admin_middle_name = profileObject.getString("middle_name");
                    String admin_last_name = profileObject.getString("last_name");
                    String admin_avatar = profileObject.getString("avatar");

                    JSONArray memberArray = groupObject.getJSONArray("group_users");
                    for (int i = 0;i<memberArray.length();i++){
                        JSONObject singleMemberObject = memberArray.getJSONObject(i);
                        String member_user_id = singleMemberObject.getString("id");
                        String member_phone = singleMemberObject.getString("phone");
                        String member_email = singleMemberObject.getString("email");

                        JSONObject memberProfileObject = singleMemberObject.getJSONObject("profile");
                        String member_first_name = memberProfileObject.getString("first_name");
                        String member_middle_name = memberProfileObject.getString("middle_name");
                        String member_last_name = memberProfileObject.getString("last_name");
                        String member_avatar = memberProfileObject.getString("avatar");
                        String member_gender = memberProfileObject.getString("gender");
                    }

                    JSONArray commentArray = groupObject.getJSONArray("comments");
                    for (int j = 0;j<commentArray.length();j++){
                        JSONObject commentObject = commentArray.getJSONObject(j);
                        String comment_id = commentObject.getString("id");
                        String comment_content = commentObject.getString("comment");
                        String created_at = commentObject.getString("created_at");

                        JSONArray profileArray = commentObject.getJSONArray("profile");
                        for (int k = 0;k<profileArray.length();k++){
                            JSONObject commentProfileObject = profileArray.getJSONObject(k);
                            String first_name = commentProfileObject.getString("first_name");
                            String middle_name = commentProfileObject.getString("middle_name");
                            String last_name = commentProfileObject.getString("last_name");
                            String avatar = commentProfileObject.getString("avatar");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        groupRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(groupRequest);
    }

    private void getChat(){
        StringRequest groupRequest = new StringRequest(Request.Method.GET, APIConfig.GROUP_URL+"/"+group_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject mainGroupObject = new JSONObject(response);
                    JSONObject groupObject = mainGroupObject.getJSONObject("group");

                    JSONArray commentArray = groupObject.getJSONArray("comments");
                    for (int j = 0;j<commentArray.length();j++){
                        JSONObject commentObject = commentArray.getJSONObject(j);
                        chat = new GroupChatModel();
                        String comment_id = commentObject.getString("id");
                        String comment_content = commentObject.getString("comment");
                        String created_at = commentObject.getString("created_at");

                        chat.setComment_id(comment_id);
                        chat.setComment_content(comment_content);
                        chat.setCreated_at(created_at);

                        JSONArray profileArray = commentObject.getJSONArray("profile");
                        for (int k = 0;k<profileArray.length();k++){
                            JSONObject commentProfileObject = profileArray.getJSONObject(k);
                            String first_name = commentProfileObject.getString("first_name");
                            String middle_name = commentProfileObject.getString("middle_name");
                            String last_name = commentProfileObject.getString("last_name");
                            String avatar = commentProfileObject.getString("avatar");
                            user_avatar = APIConfig.PREPEND_STORAGE_URL+avatar;

                            chat.setFirst_name(first_name);
                            chat.setMiddle_name(middle_name);
                            chat.setLast_name(last_name);
                            chat.setUser_avatar(user_avatar);
                        }

                        chatData.add(chat);
                    }

                    GroupChatAdapter adapter = new GroupChatAdapter(getApplicationContext(),chatData);
                    rv_comment.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        groupRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(groupRequest);
    }

    @Override
    public void onClick(View v) {
      if (v == iv_send){
         chat = new GroupChatModel();
          if (received_user_id != null){
              if (chatData!= null){
                  chat.setComment_content(et_comment.getText().toString().trim());
                  chat.setFirst_name(received_first_name);
                  chat.setLast_name(received_last_name);
                  chat.setUser_avatar(full_user_avatar_url);
                  addChat(received_user_id,et_comment.getText().toString().trim());

                  View focus = getCurrentFocus();
                  if (focus != null) {
                      ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                  }

                  et_comment.setText("");
                  hideSoftKeyboard();
                  chatData.add(chat);

                  if (adapter == null) {
                      adapter = new GroupChatAdapter(getApplicationContext(),chatData);
                      rv_comment.setAdapter(adapter);

                  } else {
                      adapter.notifyDataSetChanged();
                  }
              }
              else {
                 addChat(received_user_id,et_comment.getText().toString().trim());
                  et_comment.setText("");
                  hideSoftKeyboard();
              }
          }
      }
    }

    private void hideSoftKeyboard(){
        if (getCurrentFocus() != null){
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
    }
}
