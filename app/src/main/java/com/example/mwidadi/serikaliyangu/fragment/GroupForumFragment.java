package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.forum.GroupAdapter;
import com.example.mwidadi.serikaliyangu.forum.GroupModel;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupForumFragment extends Fragment {

    private ArrayList<GroupModel> groupData = new ArrayList<>();
    private RecyclerView rv_group;
    private ProgressBar pBar;

     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.fragment_group_forum, container, false);
         Calligrapher calligrapher = new Calligrapher(getContext());
         calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
       rv_group = (RecyclerView) v.findViewById(R.id.rv_group);
       pBar = (ProgressBar) v.findViewById(R.id.progressBar);
       rv_group.setLayoutManager(new LinearLayoutManager(getContext()));
       rv_group.setHasFixedSize(true);
       getGroup();
       return v;
    }

    private void getGroup(){
         pBar.setVisibility(View.VISIBLE);
        StringRequest groupRequest = new StringRequest(Request.Method.GET, APIConfig.GROUP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject groupObject = new JSONObject(response);
                    pBar.setVisibility(View.GONE);
                    groupData.clear();
                    JSONArray groupArray = groupObject.getJSONArray("groups");
                    for (int i = 0;i<groupArray.length();i++){
                        JSONObject singleGroupObject = groupArray.getJSONObject(i);
                        GroupModel group = new GroupModel();
                        String group_id = singleGroupObject.getString("id");
                        String user_id = singleGroupObject.getString("user_id");
                        String district_id = singleGroupObject.getString("district_id");
                        String title = singleGroupObject.getString("title");
                        String description = singleGroupObject.getString("description");
                        String created_at = singleGroupObject.getString("created_at");

                        JSONObject districtObject = singleGroupObject.getJSONObject("district");
                        String district_name = districtObject.getString("name");
                        JSONObject regionObject = districtObject.getJSONObject("region");
                        String region_id = regionObject.getString("id");
                        String region_name = regionObject.getString("name");

                        String proper_district = properCase(district_name);
                        String proper_region = properCase(region_name);

                        group.setGroup_id(group_id);
                        group.setUser_id(user_id);
                        group.setDistrict_id(district_id);
                        group.setTitle(title);
                        group.setDescription(description);
                        group.setCreated_at(created_at);
                        group.setDistrict_name(proper_district);
                        group.setRegion_id(region_id);
                        group.setRegion_name(proper_region);

                        groupData.add(group);
                    }

                    GroupAdapter adapter = new GroupAdapter(getContext(),groupData);
                    rv_group.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getActivity(). getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        groupRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(groupRequest);
    }

    public String properCase(String paramString) {
        if (paramString.length() == 0) {
            return "";
        }
        if (paramString.length() == 1) {
            return paramString.toUpperCase();
        }
        return paramString.substring(0, 1).toUpperCase() + paramString.substring(1).toLowerCase();
    }

}
