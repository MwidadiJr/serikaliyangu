package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class LeaderAdapter extends RecyclerView.Adapter<LeaderHolder> {
    Context context;
    ArrayList<LeaderModel> leaderList = new ArrayList<>();

    public LeaderAdapter(Context context, ArrayList<LeaderModel> leaderList){
        this.leaderList = leaderList;
        this.context = context;
    }

    @Override
    public LeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_leader,parent,false);
        LeaderHolder leader = new LeaderHolder(v);
        return leader;
    }

    @Override
    public void onBindViewHolder(LeaderHolder holder, int position) {
        final LeaderModel leader = leaderList.get(position);
        holder.name.setText(leader.getFirst_nsme()+" "+leader.getMiddle_name()+" "+leader.getLast_name());
        holder.title.setText(leader.getJob_title());
        holder.location.setText(leader.getRegion_name()+" - "+leader.getDistrict_name());
        Picasso.with(this.context.getApplicationContext()).load(leader.getAvatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.avatar);
        holder.cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LeaderThreadActivity.class);
                intent.putExtra("leader_item",leader);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.leaderList.size();
    }
}
