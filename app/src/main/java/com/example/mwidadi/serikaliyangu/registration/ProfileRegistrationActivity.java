package com.example.mwidadi.serikaliyangu.registration;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.MainActivity;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.AppHelper;
import com.example.mwidadi.serikaliyangu.utils.CameraPreference;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.ProfilePreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.StringFormatter;
import com.example.mwidadi.serikaliyangu.utils.UserPreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.VolleyMultipartRequest;
import com.example.mwidadi.serikaliyangu.utils.VolleySingleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ProfileRegistrationActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText et_fname,et_mname,et_lname,et_year,et_title;
    private ImageView iv_profile;
    private Button bn_next;
    private RadioGroup rg_gender;
    private RadioButton rb;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Spinner sp_region,sp_status,sp_job_status,sp_district;
    ArrayList<String> marital_status = new ArrayList<>();
    ArrayList<String> job_status = new ArrayList<>();
    private TextView tv_login;
    private static final int CAMERA_IMAGE_REQUEST = 2020;
    private static final int CAMERA_PERMISSION_REQUEST_TYPE = 3030;
    private static final String DEFAULT_VALUE = "N/A";
    private final int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private CameraPreference cameraPref;
    private String first_name,middle_name,last_name,description_title,region,
                   district,street,gender,status,birth_date,email,phone,password,c_password,village,job_ststus;
    private PreferenceManager preferenceManager;
    ProfilePreferenceManager profilePreferenceManager;
    private UserPreferenceManager userPreferenceManager;
    private String user_id;
    List<Region> regionData = new ArrayList<>();
    List<District> districtData = new ArrayList<>();
    private StringFormatter stringFormatter;
    private String district_id;
    private String profile_district_id;
    private String profile_id;
    private String profile_user_id;
    private String profile_first_name;
    private String profile_middle_name;
    private String profile_last_name;
    private String profile_avatar;
    private String profile_dob;
    private String profile_gender;
    private String profile_marital_status;
    private String profile_job_status;
    private String profile_job_title;
    private String profile_region_id;
    private String profile_district_name;
    private String profile_region_name;
    private String profile_phone;
    private String profile_email;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_register);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        cameraPref = new CameraPreference(getApplicationContext());
        preferenceManager = new PreferenceManager(this);
        profilePreferenceManager = new ProfilePreferenceManager(this);
        userPreferenceManager = new UserPreferenceManager(this);
        stringFormatter = new StringFormatter(this);
        user_id = preferenceManager.getUserId();
        mProgressDialog = new ProgressDialog(this);
        checkIfCreated();
        initView();
        getRegion();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setDatePicker();
        setUpStatusSpinner();
        setRegionSpinner();
        setDistrictSpinner();
        setUpJobSpinner();
    }

    private void initView(){
        et_year = (EditText) findViewById(R.id.et_year);
        rg_gender = (RadioGroup) findViewById(R.id.rg_gender);
        bn_next = (Button) findViewById(R.id.bn_register);
        et_fname = (EditText) findViewById(R.id.et_first_name);
        et_mname = (EditText) findViewById(R.id.et_middle_name);
        et_lname = (EditText) findViewById(R.id.et_last_name);
        et_title = (EditText) findViewById(R.id.et_title);
        sp_district = (Spinner) findViewById(R.id.sp_district);
        sp_region = (Spinner) findViewById(R.id.sp_region);
        sp_status = (Spinner) findViewById(R.id.sp_marital);
        sp_job_status = (Spinner) findViewById(R.id.sp_job_status);
        tv_login = (TextView) findViewById(R.id.tv_login);
        iv_profile = (ImageView) findViewById(R.id.iv_profile);
        rg_gender.clearCheck();
        bn_next.setOnClickListener(this);
        et_year.setOnClickListener(this);
        iv_profile.setOnClickListener(this);

        String single_status = (getString(R.string.marital_single));
        String married_status = (getString(R.string.marital_married));
        String divorced_status = (getString(R.string.marital_divorced));
        marital_status.add(single_status);
        marital_status.add(married_status);
        marital_status.add(divorced_status);

        String employed_status = (getString(R.string.job_employed));
        String entrepreneur_status = (getString(R.string.job_entrepreneur));
        String student_status =(getString(R.string.job_student));
        job_status.add(employed_status);
        job_status.add(entrepreneur_status);
        job_status.add(student_status);

        rg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                }
            }
        });
    }

    private void getChecked(){
        if (rb.getText().toString().trim().length()<1){
            Toast.makeText(getApplicationContext(),"RADIO ERROR",Toast.LENGTH_SHORT).show();
        }
        else {
            rb = (RadioButton)rg_gender.findViewById(rg_gender.getCheckedRadioButtonId());
            if (rg_gender.getCheckedRadioButtonId() == -1){
            }
        }
    }

    private void setDatePicker(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_year.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void setRegionSpinner() {
        sp_region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String region_id = regionData.get(position).getRegion_id();
                getDistrict(region_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "NOTHING HAS SELECTED", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpStatusSpinner(){
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,marital_status);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_status.setAdapter(adapter);
        sp_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                status = marital_status.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(),"NOTHING HAS SELECTED",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDistrictSpinner() {
        sp_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               district_id = districtData.get(position).getDistrict_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "NOTHING HAS SELECTED", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpJobSpinner(){
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,job_status);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_job_status.setAdapter(adapter);
        sp_job_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                job_ststus = job_status.get(i);
                if (job_ststus.equalsIgnoreCase(getString(R.string.job_entrepreneur))) {
                   et_title.setPadding(0,30,0,30);
                    et_title.setHint(getString(R.string.title_unemployed));
                }
                else if (job_ststus.equalsIgnoreCase(getString(R.string.job_employed))){
                    et_title.setPadding(5,5,5,5);
                    et_title.setHint(getString(R.string.title_employed));
                }

                else if (job_ststus.equalsIgnoreCase(getString(R.string.job_student))){
                    et_title.setPadding(5,5,5,5);
                    et_title.setHint(getString(R.string.title_student));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(),"NOTHING HAS SELECTED",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == bn_next){
            getChecked();
            validate();
        }

        else if (v == iv_profile){
            if (Build.VERSION.SDK_INT < 23) {
                showOption();
            } else {
                if (cameraPref.getCameraAllowed()) {
                    showOption();
                } else {
                    cameraPermission();
                }

            }
        }
        else if (v == et_year){
            datePickerDialog.show();
        }

    }

    private void getUserInfo(){
        first_name = et_fname.getText().toString().trim();
        middle_name = et_mname.getText().toString().trim();
        last_name = et_lname.getText().toString().trim();
        description_title = et_title.getText().toString().trim();
        gender = rb.getText().toString().trim();
        birth_date = et_year.getText().toString().trim();
    }

    private void cameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Toast.makeText(getApplicationContext(), "Enable Camera To Take Photo", Toast.LENGTH_SHORT).show();
            }
        } else {
            return;
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_TYPE);
    }

    private void showFileChooser() {
        Intent localIntent = new Intent();
        localIntent.setType("image/*");
        localIntent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(localIntent, "Select Picture"), this.PICK_IMAGE_REQUEST);
    }

    private void showOption() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("IMAGE SOURCE");
        builder.setMessage("Choose Image Source");
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_from_gallery, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
                showFileChooser();
            }
        });
        builder.setNegativeButton(R.string.text_from_camera, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                bitmap = Media.getBitmap(getContentResolver(), filePath);
                iv_profile.setImageBitmap(bitmap);

            } catch (IOException e) {
                Log.e("something", e.getMessage());
            }
        }
        if ((requestCode == CAMERA_IMAGE_REQUEST) && (resultCode == RESULT_OK)) {
            this.bitmap = ((Bitmap) data.getExtras().get("data"));
            this.iv_profile.setImageBitmap(this.bitmap);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_TYPE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraPref.setCameraAllowed(true);
                    showOption();

                } else {
                    cameraPref.setCameraAllowed(false);
                    Toast.makeText(getApplicationContext(), "You cant take photo, please enable gallery", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    private void validate(){
        getUserInfo();
        if (first_name.equalsIgnoreCase("")){
            et_fname.setError("First name can not be blank");
        }
        else if (first_name.length()<3){
            et_fname.setError("First name can must be at least 4 characters long");
        }
        else if (middle_name.equalsIgnoreCase("")){
            et_mname.setError("Middle name can not be blank");
        }
        else if (middle_name.length()<3){
            et_fname.setError("Middle name can must be at least 4 characters long");
        }
        else if (last_name.equalsIgnoreCase("")){
            et_lname.setError("Last name can not be blank");
        }
        else if (last_name.length()<3){
            et_lname.setError("Last name can must be at least 4 characters long");
        }
        else if (description_title.equalsIgnoreCase("")){
            et_title.setError("You must provide some description");
        }
        else if (et_year.getText().toString().equalsIgnoreCase("")){
            et_year.setError("Please fill this field");
        }
        else if(TextUtils.isEmpty(gender)){
            Toast.makeText(getApplicationContext(),"Please pick your gender",Toast.LENGTH_SHORT).show();
        }
        else if (district_id.length()<1){
            Toast.makeText(getApplicationContext(),"Please Pick Your Location",Toast.LENGTH_SHORT).show();
        }

        else if (status.equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(),"Please Pick Marital Status",Toast.LENGTH_SHORT).show();
        }

        else {
            getChecked();
            setProfileSession();
            registerUserProfile();
        }
    }

    private void registerUserProfile(){
        mProgressDialog.setTitle(getString(R.string.text_creating_profile));
        mProgressDialog.setMessage(getString(R.string.text_creating_profile_info));
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, APIConfig.USER_PROFILE_REFISTER_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                mProgressDialog.dismiss();
                   if(resultResponse.contains("first_name") || resultResponse.contains("marital_status")){
                        showCreated();
                  }
                  else {
                     Toast.makeText(getApplicationContext(),getResources().getString(R.string.failed_label),Toast.LENGTH_SHORT).show();
               }
                try {
                    JSONObject userInformationObject = new JSONObject(resultResponse);
                    JSONObject userObject = userInformationObject.getJSONObject("profile");
                    profile_id = userObject.getString("id");
                    profile_user_id = userObject.getString("user_id");
                    profile_district_id = userObject.getString("district_id");
                    profile_first_name = userObject.getString("first_name");
                    profile_middle_name = userObject.getString("middle_name");
                    profile_last_name = userObject.getString("last_name");
                    profile_avatar = userObject.getString("avatar");
                    profile_dob = userObject.getString("dob");
                    profile_gender = userObject.getString("gender");
                    profile_marital_status = userObject.getString("marital_status");
                    profile_job_status = userObject.getString("job_status");
                    profile_job_title = userObject.getString("job_title");

                    JSONObject districtObject = userObject.getJSONObject("district");
                    profile_district_id = districtObject.getString("id");
                    profile_region_id = districtObject.getString("region_id");
                    profile_district_name = districtObject.getString("name");

                    JSONObject regionObject = districtObject.getJSONObject("region");
                    profile_region_name = regionObject.getString("name");

                    JSONObject profileObject  = userObject.getJSONObject("user");
                    profile_phone = profileObject.getString("phone");
                    profile_email = profileObject.getString("email");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isUserExist(profile_user_id)){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.failed_label),Toast.LENGTH_SHORT).show();
                }
                else {
                    setPreferenceSession();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_FIRST_NAME,first_name);
                params.put(APIConfig.KEY_MIDDLE_NAME,middle_name);
                params.put(APIConfig.KEY_LAST_NAME,last_name);
                params.put(APIConfig.KEY_DOB,birth_date);
                params.put(APIConfig.KEY_GENDER,gender);
                params.put(APIConfig.KEY_JOB_STATUS,job_ststus);
                params.put(APIConfig.KEY_JOB_TITLE,description_title);
                params.put(APIConfig.KEY_MARITAL_STATUS,status);
                params.put(APIConfig.KEY_DISTRICT_ID,district_id);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put(APIConfig.KEY_PROFILE_AVATAR, new DataPart(first_name+".jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), iv_profile.getDrawable()), "image/jpeg"));
                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    private void setProfileSession(){
        profilePreferenceManager = new ProfilePreferenceManager(this);
        profilePreferenceManager.createProfile(first_name,user_id);
        profilePreferenceManager.setIsCreated(true);
        profilePreferenceManager.setUserId(user_id);
        profilePreferenceManager.setFirstName(first_name);
    }

    private void setPreferenceSession() {
        if (TextUtils.isEmpty(profile_phone)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_email)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_avatar)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_dob)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_district_id)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_region_id)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_district_name)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_region_name)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_gender)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_marital_status)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_job_status)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_job_title)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(profile_id)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(user_id)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_label), Toast.LENGTH_SHORT).show();
        }
        else {
            preferenceManager = new PreferenceManager(this);
            preferenceManager.setFirstName(et_fname.getText().toString());
            preferenceManager.setMiddleName(et_mname.getText().toString());
            preferenceManager.setLastName(et_lname.getText().toString());
            preferenceManager.setEmail(profile_email);
            preferenceManager.setAvatar(profile_avatar);
            preferenceManager.setBirthDay(profile_dob);
            preferenceManager.setDistrictId(profile_district_id);
            preferenceManager.setRegionId(profile_region_id);
            preferenceManager.setDistrictName(profile_district_name);
            preferenceManager.setRegionName(profile_region_name);
            preferenceManager.setGender(profile_gender);
            preferenceManager.setMaritalStatus(profile_marital_status);
            preferenceManager.setJobTitle(profile_job_title);
            preferenceManager.setJobStatus(profile_job_status);
            preferenceManager.setProfileId(profile_id);
            preferenceManager.setUserId(user_id);
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        }
    }

    private void checkIfCreated(){
        if (profilePreferenceManager.getCreated()){
            Intent mainIntent = new Intent(ProfileRegistrationActivity.this,MainActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainIntent);
            finish();
        }
    }

    private void getRegion(){
        final StringRequest regionRequest = new StringRequest(Request.Method.GET, APIConfig.ALL_REGION_URL, new Response.Listener<String>() {
            JSONObject regionObject = null;
            @Override
            public void onResponse(String response) {
                try {
                    regionObject = new JSONObject(response);
                    JSONArray regionArray = regionObject.getJSONArray("regions");
                    for (int i = 0;i<regionArray.length();i++){
                        JSONObject regionSingleObject = regionArray.getJSONObject(i);
                        Region region = new Region();
                        String region_id = regionSingleObject.getString("id");
                        String region_name = regionSingleObject.getString("name");
                        String created_at = regionSingleObject.getString("created_at");
                        String updated_at = regionSingleObject.getString("updated_at");
                        String formatted_region_name = stringFormatter.properCase(region_name);
                        region.setRegion_name(formatted_region_name);
                        region.setRegion_createc_at(created_at);
                        region.setRegion_updated_at(updated_at);
                        region.setRegion_id(region_id);

                        regionData.add(region);
                    }

                    RegionAdapter adapter = new RegionAdapter(getApplicationContext(), regionData);
                    sp_region.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        regionRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(regionRequest);
    }

    private void getDistrict(final String region_id){
        StringRequest districtRequest = new StringRequest(Request.Method.GET, APIConfig.ALL_DISTRICT_URL+region_id, new Response.Listener<String>() {
            JSONObject districtObject = null;
            @Override
            public void onResponse(String response) {
                try {
                    districtObject = new JSONObject(response);
                    JSONObject regionObject = districtObject.getJSONObject("region");
                    JSONArray districtArray = regionObject.getJSONArray("districts");
                    districtData.clear();
                    for (int i = 0;i<districtArray.length();i++){
                        District district = new District();
                        JSONObject singleDistrictObject = districtArray.getJSONObject(i);
                        String district_id = singleDistrictObject.getString("id");
                        String region_id = singleDistrictObject.getString("region_id");
                        String district_name = singleDistrictObject.getString("name");
                        String district_created_at = singleDistrictObject.getString("created_at");
                        String district_updated_at = singleDistrictObject.getString("updated_at");
                        String formatted_district_name = stringFormatter.properCase(district_name);
                        district.setRegion_id(region_id);
                        district.setDistrict_created_at(district_created_at);
                        district.setDistrict_updated_at(district_updated_at);
                        district.setDistrict_name(formatted_district_name);
                        district.setDistrict_id(district_id);
                        districtData.add(district);
                    }

                    DistrictAdapter adapter = new DistrictAdapter(getApplicationContext(), districtData);
                    sp_district.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        districtRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(districtRequest);
    }

    private boolean isUserExist(String user_id){
        if (user_id.length()<0){
            return true;
        }
        else {
            return false;
        }
    }

    private void showCreated() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.text_successfully_created));
        builder.setMessage(getString(R.string.messasge_successfully_created_label));
        builder.setIcon(R.drawable.ic_action_ok);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });
        builder.show();
    }

}
