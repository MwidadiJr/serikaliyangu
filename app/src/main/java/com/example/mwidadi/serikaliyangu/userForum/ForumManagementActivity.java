package com.example.mwidadi.serikaliyangu.userForum;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ForumManagementActivity extends AppCompatActivity {

    private RecyclerView rv_post;
    private String forum_id;
    private String user_id;
    private String forum_title;
    private String forum_post;
    private String forum_attachment;
    private String created_at;
    ArrayList<MyForum> forumData = new ArrayList<>();
    UserForumAdapter adapter;
    private CharSequence timePassedString;
    private PreferenceManager preferenceManager;
    private String ago_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_management);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.text_my_post);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        preferenceManager = new PreferenceManager(this);
        String user_id = preferenceManager.getUserId();
        rv_post = (RecyclerView) findViewById(R.id.rv_post);
        rv_post.setLayoutManager(new LinearLayoutManager(this));
        getForum(user_id);
    }

    private void getForum(final String your_id){
        StringRequest forumRequest = new StringRequest(Request.Method.GET, APIConfig.GET_USER_FORUM_URL+your_id+"/forums", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject userObject = new JSONObject(response);
                    JSONObject singleUserObject = userObject.getJSONObject("user");
                    JSONArray forumArray = singleUserObject.getJSONArray("forums");
                    for (int i = 0;i<forumArray.length();i++){
                        JSONObject forumObject = forumArray.getJSONObject(i);
                        MyForum forum = new MyForum();
                        forum_id = forumObject.getString("id");
                        user_id = forumObject.getString("user_id");
                        forum_title = forumObject.getString("title");
                        forum_post = forumObject.getString("post");
                        forum_attachment = forumObject.getString("attachment");
                        created_at = forumObject.getString("created_at");

                        CharSequence formatedTime = formatTime(created_at);
                        ago_time = formatedTime.toString();

                        forum.setForum_id(forum_id);
                        forum.setTitle(forum_title);
                        forum.setPost(forum_post);
                        forum.setAttachment(forum_attachment);
                        forum.setUser_id(user_id);
                        forum.setCreated_at(ago_time);
                        forumData.add(forum);
                    }

                    adapter = new UserForumAdapter(getApplicationContext(), forumData);
                    rv_post.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        forumRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(forumRequest);
    }

    private CharSequence formatTime(String dateTime){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date  = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString (time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }
}
