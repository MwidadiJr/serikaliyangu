package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {

    ViewPagerAdapter viewPagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.chat_label);
        TabLayout tabLayout = (TabLayout)v.findViewById(R.id.tabLayout);
        ViewPager viewPager = (ViewPager)v.findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragments(new UserFragment(),"USERS");
        viewPagerAdapter.addFragments(new AllChatFragment(),"CHAT");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setHasOptionsMenu(true);
        return v;
    }

}
