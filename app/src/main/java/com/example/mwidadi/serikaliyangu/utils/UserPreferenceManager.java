package com.example.mwidadi.serikaliyangu.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mwidadi on 11/29/2017.
 */

public class UserPreferenceManager {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "user_information";
    private static final String USER_ID = "user_id";
    private static final String PHONE = "phone";
    private static final String IS_CREATED = "created";

    public UserPreferenceManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();

    }

    public void createProfile(String phone, String user_id) {
        editor.putString(PHONE, phone);
        editor.putString(USER_ID, user_id);
        editor.commit();
    }

    public void setIsCreated(boolean isCreated){
        editor.putBoolean(IS_CREATED,isCreated);
        editor.commit();
    }

    public boolean getCreated(){
        return preferences.getBoolean(IS_CREATED,false);
    }


    public void setUserId(String userId){
        editor.putString(USER_ID,userId);
        editor.commit();
    }

    public String getUserId() {
        return preferences.getString(USER_ID, null);
    }

    public void setPhone(String phone){
        editor.putString(PHONE,phone);
        editor.commit();
    }
}
