package com.example.mwidadi.serikaliyangu;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import com.example.mwidadi.serikaliyangu.fragment.ChallengeFragment;
import com.example.mwidadi.serikaliyangu.fragment.HomeFragment;
import com.example.mwidadi.serikaliyangu.fragment.ProfileFragment;
import com.example.mwidadi.serikaliyangu.fragment.TrainingFragment;
import com.example.mwidadi.serikaliyangu.fragment.UserForumFragment;
import com.example.mwidadi.serikaliyangu.utils.LanguagePreference;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;
import java.util.Locale;
import me.anwarshahriar.calligrapher.Calligrapher;

public class MainActivity extends AppCompatActivity {

    BottomBar bottomBar;
    private LanguagePreference languagePreference;
    private String currentLanguage;
    private Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getCurrentLanguage();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);

        bottomBar = BottomBar.attach(this,savedInstanceState);
        bottomBar.useFixedMode();
        bottomBar.noTabletGoodness();
        bottomBar.setTextAppearance(R.style.bottomBarTextView);
        bottomBar.setItemsFromMenu(R.menu.main_bottom_navigation, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.nav_home){
                    HomeFragment hf = new HomeFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,hf).commit();
                }
                else if (menuItemId == R.id.nav_forum){
                    UserForumFragment ff = new UserForumFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,ff).commit();
                }

                else if (menuItemId == R.id.nav_training){
                    TrainingFragment tf = new TrainingFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,tf).commit();
                }
                else if (menuItemId == R.id.nav_challenge){
                    ChallengeFragment chf = new ChallengeFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,chf).commit();
                }

                else if (menuItemId == R.id.nav_profile){
                    ProfileFragment pf = new ProfileFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,pf).commit();
                }
            }

            @Override
            public void onMenuTabReSelected(int menuItemId) {

            }

        });
        bottomBar.mapColorForTab(0,"#80bce9");
        bottomBar.mapColorForTab(1,"#80bce9");
        bottomBar.mapColorForTab(2,"#80bce9");
        bottomBar.mapColorForTab(3,"#80bce9");
        bottomBar.mapColorForTab(4,"#80bce9");
        bottomBar.setActiveTabColor(Color.BLUE);
    }

    private void share() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,"Here we gonna put put some links or any shared words from thinkerstech");
        startActivity(Intent.createChooser(shareIntent, "Share Serikali Yangu"));
    }

    boolean twice;

    @Override
    public void onBackPressed() {

        if (twice == true) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
        }
        twice = true;

        Toast.makeText(getApplicationContext(), "Please press BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                twice = false;
            }
        }, 2000);
    }

    private void getCurrentLanguage(){
        languagePreference = new LanguagePreference(getApplicationContext());
        currentLanguage = languagePreference.getLanguage();
        if (currentLanguage.equalsIgnoreCase("sw")) {
            Locale locale = new Locale("sw");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        } else {
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }
}
