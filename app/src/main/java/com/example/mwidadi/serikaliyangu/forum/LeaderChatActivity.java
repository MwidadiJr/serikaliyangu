package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class LeaderChatActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText et_chat;
    private RecyclerView rv_chat;
    private ImageButton iv_send;
    private CircleImageView chat_add_btn;
    private ThreadModel thread;
    private String thread_id;
    private String full_name;
    private PreferenceManager preferenceManager;
    private String user_id;
    private String received_first_name;
    private String received_middle_name;
    private String received_last_name;
    private String received_user_id;
    private String received_user_avatar;
    private String full_user_avatar_url = "";
    private String user_avatar = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_chat);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change,R.anim.slide_down);
                }
            });
        }
        preferenceManager = new PreferenceManager(getApplicationContext());
        user_id = preferenceManager.getUserId();
        received_first_name = preferenceManager.getFirstName();
        received_middle_name = preferenceManager.getMiddleName();
        received_last_name = preferenceManager.getLastName();
        received_user_id = preferenceManager.getUserId();
        received_user_avatar = preferenceManager.getAvatar();
        full_user_avatar_url = APIConfig.PREPEND_STORAGE_URL+received_user_avatar;
        initView();
        getThreadData();
        getMessage();
    }

    private void initView(){
        et_chat = (EditText) findViewById(R.id.et_chat);
        iv_send = (ImageButton) findViewById(R.id.iv_send);
        rv_chat = (RecyclerView) findViewById(R.id.rv_chat);
        chat_add_btn = (CircleImageView) findViewById(R.id.chat_add_btn);
        Picasso.with(getApplicationContext()).load(full_user_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(chat_add_btn);
        rv_chat.setLayoutManager(new LinearLayoutManager(this));
        rv_chat.setHasFixedSize(true);
        iv_send.setOnClickListener(this);
    }

    private void getThreadData() {
        thread = (ThreadModel) getIntent().getSerializableExtra("thread_item");
        thread_id = thread.getThread_id();
        full_name = thread.getFirst_name()+" "+thread.getLast_name();
        getSupportActionBar().setTitle(full_name);
    }

    private void getMessage(){
        StringRequest messageRequest = new StringRequest(Request.Method.GET, APIConfig.GET_MESSAGE_URL+thread_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject mainThreadObject = new JSONObject(response);
                    JSONObject threadObject = mainThreadObject.getJSONObject("threads");
                    String thread_id = threadObject.getString("id");
                    String initial_message = threadObject.getString("message");

                    JSONArray messageArray = threadObject.getJSONArray("messages");
                    for (int i = 0;i<messageArray.length();i++){
                        JSONObject singleMessageObject = messageArray.getJSONObject(i);
                        String message_id = singleMessageObject.getString("id");
                        String message_content = singleMessageObject.getString("message");
                        String created_at = singleMessageObject.getString("created_at");
                        String user_id = singleMessageObject.getString("sender_id");

                        Toast.makeText(getApplicationContext(),message_content,Toast.LENGTH_LONG).show();

                        JSONObject userObject = singleMessageObject.getJSONObject("user");
                        JSONObject profileObject = userObject.getJSONObject("profile");
                        String first_name = profileObject.getString("first_name");
                        String middle_name = profileObject.getString("middle_name");
                        String last_name = profileObject.getString("last_name");
                        String avatar = profileObject.getString("avatar");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        messageRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(messageRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == iv_send){

        }
    }
}
