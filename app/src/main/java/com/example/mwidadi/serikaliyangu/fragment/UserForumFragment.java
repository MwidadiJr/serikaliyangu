package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserForumFragment extends Fragment {

    ViewPagerAdapter viewPagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_user_forum, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        TabLayout tabLayout = (TabLayout)v.findViewById(R.id.tabLayout);
        ViewPager viewPager = (ViewPager)v.findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragments(new GroupForumFragment(),getResources().getString(R.string.discussion_label));
        viewPagerAdapter.addFragments(new AllForumFragment(),getResources().getString(R.string.all_forum_label));
        viewPagerAdapter.addFragments(new LeaderFragment(),getResources().getString(R.string.leaders_label));
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }

}
