package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class GroupChatHolder extends RecyclerView.ViewHolder {
    ImageView userImage;
    TextView user_first_name;
    TextView commnet;
    TextView time;
    CardView cvCard;

    public GroupChatHolder(View itemView) {
        super(itemView);
        userImage = (ImageView) itemView.findViewById(R.id.iv_avatar);
        user_first_name = (TextView) itemView.findViewById(R.id.tv_sender_fname);
        commnet = (TextView) itemView.findViewById(R.id.tv_comment);
        time = (TextView) itemView.findViewById(R.id.tv_time);
        cvCard = (CardView) itemView.findViewById(R.id.cv_comment);
    }
}
