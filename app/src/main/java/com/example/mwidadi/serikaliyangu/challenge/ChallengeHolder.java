package com.example.mwidadi.serikaliyangu.challenge;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class ChallengeHolder extends RecyclerView.ViewHolder {
    ImageView avatar;
    ImageView image_post;
    TextView content;
    TextView creator;
    TextView time;
    TextView title;
    CardView cvCard;
    TextView tv_prize;
    TextView tv_deadline;
    TextView tv_category;
    Button bn_participate;

    public ChallengeHolder(View itemView) {
        super(itemView);
        avatar = (ImageView) itemView.findViewById(R.id.iv_logo);
        image_post = (ImageView) itemView.findViewById(R.id.iv_post);
        content = (TextView) itemView.findViewById(R.id.tv_content);
        title = (TextView) itemView.findViewById(R.id.tv_title);
        tv_prize = (TextView) itemView.findViewById(R.id.tv_prize);
        tv_deadline = (TextView) itemView.findViewById(R.id.tv_deadline);
        tv_category = (TextView) itemView.findViewById(R.id.tv_category);
        creator = (TextView) itemView.findViewById(R.id.tv_sender);
        time = (TextView) itemView.findViewById(R.id.tv_time);
        cvCard = (CardView) itemView.findViewById(R.id.cv_forum);
        bn_participate = (Button) itemView.findViewById(R.id.bn_participate);
    }
}
