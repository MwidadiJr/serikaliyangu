package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class MemberHolder extends RecyclerView.ViewHolder {
    ImageView avatar;
    TextView name;
    TextView phone;
    TextView email;
    TextView gender;
    CardView cvCard;

    public MemberHolder(View itemView) {
        super(itemView);
        avatar = (ImageView) itemView.findViewById(R.id.iv_image);
        name = (TextView) itemView.findViewById(R.id.tv_name);
        phone = (TextView) itemView.findViewById(R.id.tv_phone);
        email = (TextView) itemView.findViewById(R.id.tv_email);
        gender = (TextView) itemView.findViewById(R.id.tv_gender);
        cvCard = (CardView) itemView.findViewById(R.id.cv_user);
    }
}
