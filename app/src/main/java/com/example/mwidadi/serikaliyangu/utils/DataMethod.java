package com.example.mwidadi.serikaliyangu.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Mwidadi on 11/26/2017.
 */

public class DataMethod {

    public Context mContext;
    public  DataMethod(Context context){
        this.mContext = context;
    }

    public void printKeyHash(){
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo("com.example.mwidadi.serikaliyangu", PackageManager.GET_SIGNATURES);
            for (Signature signature:info.signatures){
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("Key Hash", Base64.encodeToString(md.digest(),Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
