package com.example.mwidadi.serikaliyangu.profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.registration.District;
import com.example.mwidadi.serikaliyangu.registration.DistrictAdapter;
import com.example.mwidadi.serikaliyangu.registration.Region;
import com.example.mwidadi.serikaliyangu.registration.RegionAdapter;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.AppHelper;
import com.example.mwidadi.serikaliyangu.utils.CameraPreference;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.StringFormatter;
import com.example.mwidadi.serikaliyangu.utils.VolleyMultipartRequest;
import com.example.mwidadi.serikaliyangu.utils.VolleySingleton;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import me.anwarshahriar.calligrapher.Calligrapher;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageView iv_avatar;
    private EditText et_title,et_password,et_email,et_phone,et_first_name,et_middle_name,et_last_name,et_dob;
    private Spinner sp_regin,sp_district,sp_job_status,sp_marital_status;
    private TextView tv_privacy;
    private Button bn_region,bn_district,bn_fname,bn_mname,bn_lname,bn_password,bn_email,bn_phone,bn_job_status,bn_title,bn_marital,bn_dob,bn_save;
    private PreferenceManager preferenceManager;
    private String first_name;
    private String last_name;
    private String middle_name;
    private String phone_number;
    private String email;
    private String gender;
    private String received_job_status;
    private String job_title;
    private String avatar;
    private String birth_day;
    private String password;
    private Bitmap bitmap;
    private String profile_id;
    private CameraPreference cameraPref;
    private static final int CAMERA_IMAGE_REQUEST = 2020;
    private static final int CAMERA_PERMISSION_REQUEST_TYPE = 3030;
    private final int PICK_IMAGE_REQUEST = 1;
    private String full_avatar_url = "";
    private StringFormatter stringFormatter;
    private String region_name;
    List<Region> regionData = new ArrayList<>();
    List<District> districtData = new ArrayList<>();
    private String district_id;
    ArrayList<String> marital_status = new ArrayList<>();
    ArrayList<String> job_status = new ArrayList<>();
    private String job_ststus;
    private String status;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.text_profile_edit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });
        }
        preferenceManager = new PreferenceManager(this);
        stringFormatter = new StringFormatter(this);
        cameraPref = new CameraPreference(this);
        getRegion();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setDatePicker();
        initView();
        getUserInfo();
        setRegionSpinner();
        setDistrictSpinner();
        setUpJobSpinner();
        setUpStatusSpinner();
        setInfo();

    }

    private void initView(){
        iv_avatar = (ImageView) findViewById(R.id.iv_avatar);
        et_title = (EditText) findViewById(R.id.et_job_title);
        et_password = (EditText) findViewById(R.id.et_password);
        et_email = (EditText) findViewById(R.id.et_email);
        et_phone = (EditText) findViewById(R.id.et_phone);
        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_middle_name = (EditText) findViewById(R.id.et_middle_name);
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        sp_job_status = (Spinner) findViewById(R.id.sp_job_status);
        sp_regin = (Spinner) findViewById(R.id.sp_region);
        et_dob = (EditText) findViewById(R.id.et_dob);
        sp_district = (Spinner) findViewById(R.id.sp_district);
        sp_marital_status = (Spinner) findViewById(R.id.sp_marital_status);
        bn_email = (Button) findViewById(R.id.bn_email);
        bn_region = (Button) findViewById(R.id.bn_region);
        bn_district = (Button) findViewById(R.id.bn_district);
        bn_fname = (Button) findViewById(R.id.bn_fname);
        bn_mname = (Button) findViewById(R.id.bn_mname);
        bn_lname = (Button) findViewById(R.id.bn_lname);
        bn_password = (Button) findViewById(R.id.bn_password);
        bn_phone = (Button) findViewById(R.id.bn_phone);
        bn_job_status = (Button) findViewById(R.id.bn_job_status);
        bn_title = (Button) findViewById(R.id.bn_title);
        bn_marital = (Button) findViewById(R.id.bn_marital);
        bn_dob = (Button) findViewById(R.id.bn_dob);
        bn_save = (Button) findViewById(R.id.bn_save);
        bn_region.setOnClickListener(this);
        bn_district.setOnClickListener(this);
        bn_fname.setOnClickListener(this);
        bn_mname.setOnClickListener(this);
        bn_lname.setOnClickListener(this);
        bn_password.setOnClickListener(this);
        bn_job_status.setOnClickListener(this);
        bn_title.setOnClickListener(this);
        bn_marital.setOnClickListener(this);
        bn_phone.setOnClickListener(this);
        bn_dob.setOnClickListener(this);
        bn_save.setOnClickListener(this);
        iv_avatar.setOnClickListener(this);
        et_dob.setOnClickListener(this);

        String employed_status = (getString(R.string.job_employed));
        String entrepreneur_status = (getString(R.string.job_entrepreneur));
        String student_status =(getString(R.string.job_student));
        job_status.add(employed_status);
        job_status.add(entrepreneur_status);
        job_status.add(student_status);

        String single_status = (getString(R.string.marital_single));
        String married_status = (getString(R.string.marital_married));
        String divorced_status = (getString(R.string.marital_divorced));
        marital_status.add(single_status);
        marital_status.add(married_status);
        marital_status.add(divorced_status);
    }

    private void setInfo(){
        Picasso.with(this.getApplicationContext()).load(full_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(iv_avatar);
        et_title.setText(job_title);
        et_phone.setText(phone_number);
        et_email.setText(email);
        et_password.setText(password);
        et_first_name.setText(first_name);
        et_middle_name.setText(middle_name);
        et_last_name.setText(last_name);
        et_dob.setText(birth_day);
    }

    private void getUserInfo(){
        first_name = stringFormatter.properCase(preferenceManager.getFirstName());
        middle_name = stringFormatter.properCase(preferenceManager.getMiddleName());
        last_name = stringFormatter.properCase(preferenceManager.getLastName());
        phone_number = "0"+preferenceManager.getPhone();
        email = preferenceManager.getEmail();
        gender = preferenceManager.getGender();
        birth_day = preferenceManager.getBirthDay();
        job_title = stringFormatter.properCase(preferenceManager.getJobTitle());
        received_job_status = preferenceManager.getJobStatus();
        avatar = preferenceManager.getAvatar();
        password = preferenceManager.getPassword();
        profile_id = preferenceManager.getProfileId();
        region_name = stringFormatter.properCase(preferenceManager.getRegionName());
        full_avatar_url = APIConfig.PREPEND_STORAGE_URL+avatar;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_avatar:
                if (Build.VERSION.SDK_INT < 23) {
                    showOption();
                } else {
                    if (cameraPref.getCameraAllowed()) {
                        showOption();
                    } else {
                        cameraPermission();
                    }

                }
                break;

            case R.id.bn_email:
                break;

            case R.id.bn_phone:
                break;

            case R.id.bn_job_status:
                break;

            case R.id.bn_marital:
                break;

            case R.id.bn_password:
                break;

            case R.id.bn_save:
                editProfile(profile_id);
                break;

            case R.id.bn_fname:
                break;

            case R.id.et_dob:
                datePickerDialog.show();
           }
      }

    private void showOption() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("IMAGE SOURCE");
        builder.setMessage("Choose Image Source");
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_from_gallery, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
                showFileChooser();
            }
        });
        builder.setNegativeButton(R.string.text_from_camera, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iv_avatar.setImageBitmap(bitmap);

            } catch (IOException e) {
                Log.e("something", e.getMessage());
            }
        }
        if ((requestCode == CAMERA_IMAGE_REQUEST) && (resultCode == RESULT_OK)) {
            this.bitmap = ((Bitmap) data.getExtras().get("data"));
            this.iv_avatar.setImageBitmap(this.bitmap);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_TYPE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraPref.setCameraAllowed(true);
                    showOption();

                } else {
                    cameraPref.setCameraAllowed(false);
                    Toast.makeText(getApplicationContext(), "You cant take photo, please enable gallery", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    private void cameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Toast.makeText(getApplicationContext(), "Enable Camera To Take Photo", Toast.LENGTH_SHORT).show();
            }
        } else {
            return;
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_TYPE);
    }

    private void showFileChooser() {
        Intent localIntent = new Intent();
        localIntent.setType("image/*");
        localIntent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(localIntent, "Select Picture"), this.PICK_IMAGE_REQUEST);
    }

    private void editProfile(final String user_id){
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, APIConfig.EDIT_USER_PROFILE_URL+user_id, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject userInformationObject = new JSONObject(resultResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_FIRST_NAME,et_first_name.getText().toString().trim());
                params.put(APIConfig.KEY_MIDDLE_NAME,et_middle_name.getText().toString().trim());
                params.put(APIConfig.KEY_LAST_NAME,et_last_name.getText().toString().trim());
                params.put(APIConfig.KEY_DOB,et_dob.getText().toString().trim());
                params.put(APIConfig.KEY_GENDER,gender);
                params.put(APIConfig.KEY_JOB_STATUS,job_ststus);
                params.put(APIConfig.KEY_JOB_TITLE,et_title.getText().toString().trim());
                params.put(APIConfig.KEY_MARITAL_STATUS,status);
                params.put(APIConfig.KEY_DISTRICT_ID,district_id);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put(APIConfig.KEY_PROFILE_AVATAR, new DataPart(first_name+".jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), iv_avatar.getDrawable()), "image/jpeg"));
                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    private void getRegion(){
        final StringRequest regionRequest = new StringRequest(Request.Method.GET, APIConfig.ALL_REGION_URL, new Response.Listener<String>() {
            JSONObject regionObject = null;
            @Override
            public void onResponse(String response) {
                try {
                    regionObject = new JSONObject(response);
                    JSONArray regionArray = regionObject.getJSONArray("regions");
                    for (int i = 0;i<regionArray.length();i++){
                        JSONObject regionSingleObject = regionArray.getJSONObject(i);
                        Region region = new Region();
                        String region_id = regionSingleObject.getString("id");
                        String region_name = regionSingleObject.getString("name");
                        String created_at = regionSingleObject.getString("created_at");
                        String updated_at = regionSingleObject.getString("updated_at");

                        String formatted_region_name = stringFormatter.properCase(region_name);
                        region.setRegion_name(formatted_region_name);
                        region.setRegion_createc_at(created_at);
                        region.setRegion_updated_at(updated_at);
                        region.setRegion_id(region_id);

                        regionData.add(region);
                    }

                    RegionAdapter adapter = new RegionAdapter(getApplicationContext(), regionData);
                    sp_regin.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        regionRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(regionRequest);
    }

    private void getDistrict(final String region_id){
        StringRequest districtRequest = new StringRequest(Request.Method.GET, APIConfig.ALL_DISTRICT_URL+region_id, new Response.Listener<String>() {
            JSONObject districtObject = null;
            @Override
            public void onResponse(String response) {
                try {
                    districtObject = new JSONObject(response);
                    JSONObject regionObject = districtObject.getJSONObject("region");
                    JSONArray districtArray = regionObject.getJSONArray("districts");
                    districtData.clear();
                    for (int i = 0;i<districtArray.length();i++){
                        District district = new District();
                        JSONObject singleDistrictObject = districtArray.getJSONObject(i);
                        String district_id = singleDistrictObject.getString("id");
                        String region_id = singleDistrictObject.getString("region_id");
                        String district_name = singleDistrictObject.getString("name");
                        String district_created_at = singleDistrictObject.getString("created_at");
                        String district_updated_at = singleDistrictObject.getString("updated_at");
                        String formatted_district_name = stringFormatter.properCase(district_name);
                        district.setRegion_id(region_id);
                        district.setDistrict_created_at(district_created_at);
                        district.setDistrict_updated_at(district_updated_at);
                        district.setDistrict_name(formatted_district_name);
                        district.setDistrict_id(district_id);
                        districtData.add(district);
                    }

                    DistrictAdapter adapter = new DistrictAdapter(getApplicationContext(), districtData);
                    sp_district.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        districtRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(districtRequest);
    }

    private void setRegionSpinner() {
        sp_regin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String region_id = regionData.get(position).getRegion_id();
                getDistrict(region_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "NOTHING HAS SELECTED", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDistrictSpinner() {
        sp_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                district_id = districtData.get(position).getDistrict_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "NOTHING HAS SELECTED", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpJobSpinner(){
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,job_status);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_job_status.setAdapter(adapter);
        sp_job_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                job_ststus = job_status.get(i);
                if (job_ststus.equalsIgnoreCase(getString(R.string.job_entrepreneur))) {
                    et_title.setPadding(5,5,5,5);
                    et_title.setHint(getString(R.string.title_unemployed));
                }
                else if (job_ststus.equalsIgnoreCase(getString(R.string.job_employed))){
                    et_title.setPadding(5,5,5,5);
                    et_title.setHint(getString(R.string.title_employed));
                }

                else if (job_ststus.equalsIgnoreCase(getString(R.string.job_student))){
                    et_title.setPadding(5,5,5,5);
                    et_title.setHint(getString(R.string.title_student));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(),"NOTHING HAS SELECTED",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpStatusSpinner(){
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,marital_status);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_marital_status.setAdapter(adapter);
        sp_marital_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                status = marital_status.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(),"NOTHING HAS SELECTED",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDatePicker(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_dob.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

}
