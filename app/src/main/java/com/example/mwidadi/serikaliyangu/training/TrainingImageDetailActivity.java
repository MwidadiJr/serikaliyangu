package com.example.mwidadi.serikaliyangu.training;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.FileOutputStream;
import me.anwarshahriar.calligrapher.Calligrapher;

public class TrainingImageDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_image;
    private TextView tv_title, tv_description, tv_sender, tv_time;
    private Training training;
    private CoordinatorLayout cl_main;
    private Button bn_view;
    private String training_title;
    private String training_content;
    private String posted_time;
    private String first_name;
    private String training_image;
    private String middle_name;
    private String last_name;
    private String training_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_image_detail);
        getInfo();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        initCollapsingToolbar();
        initView();
        setInfo();
    }

    private void initView() {
        iv_image = (ImageView) findViewById(R.id.iv_image);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_sender = (TextView) findViewById(R.id.tv_author);
        tv_time = (TextView) findViewById(R.id.tv_time);
        bn_view = (Button) findViewById(R.id.bn_view);
        cl_main = (CoordinatorLayout) findViewById(R.id.cl_main);
        bn_view.setOnClickListener(this);
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(getString(R.string.forum_detail_label));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void getInfo() {
        training = (Training) getIntent().getSerializableExtra("training_item");
        first_name = training.getFirst_name();
        middle_name = training.getMiddle_name();
        last_name = training.getLast_name();
        training_content = training.getTraining_post();
        training_title = training.getTraining_title();
        posted_time = training.getCreated_at();
        training_image = training.getAttachment_content();
        training_id = training.getTraining_id();
    }

    private void setInfo() {
        tv_time.setText(posted_time);
        tv_sender.setText(first_name + " " + middle_name + " " + last_name);
        tv_description.setText(training_content);
        tv_title.setText(training_title);
        Picasso.with(getApplicationContext()).load(training_image)
                .placeholder(R.drawable.ic_training_demo)
                .into(iv_image);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bn_view:
                Intent courseIntent = new Intent(getApplicationContext(), CourseActivity.class);
                courseIntent.putExtra("training_id", training_id);
                startActivity(courseIntent);
                break;
            default:
        }
    }

    private void share() {
        Bitmap bitmap = getBitmapFromView(cl_main);
        try {
            File file = new File(getCacheDir(), "serikaliyangutraining" + ".png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share training post via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }
}
