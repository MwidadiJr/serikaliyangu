package com.example.mwidadi.serikaliyangu.blog;

import java.io.Serializable;

/**
 * Created by Mwidadi on 11/7/2017.
 */

public class BlogPost implements Serializable{
   private String post_id;
   private String user_id;
   private String post_title;
   private String post_content;
   private String post_created;
   private String post_updated;
   private String user_phone;
   private String user_email;
   private String district_id;
   private String first_name;
   private String middle_name;
   private String last_name;
   private String user_avatar;
   private String user_dob;
   private String user_gender;
   private String user_marital_status;
   private String user_job_status;
   private String user_job_title;
   private String user_district;
   private String attachment_id;
   private String attachment;

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_created() {
        return post_created;
    }

    public void setPost_created(String post_created) {
        this.post_created = post_created;
    }

    public String getPost_updated() {
        return post_updated;
    }

    public void setPost_updated(String post_updated) {
        this.post_updated = post_updated;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_dob() {
        return user_dob;
    }

    public void setUser_dob(String user_dob) {
        this.user_dob = user_dob;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_marital_status() {
        return user_marital_status;
    }

    public void setUser_marital_status(String user_marital_status) {
        this.user_marital_status = user_marital_status;
    }

    public String getUser_job_status() {
        return user_job_status;
    }

    public void setUser_job_status(String user_job_status) {
        this.user_job_status = user_job_status;
    }

    public String getUser_job_title() {
        return user_job_title;
    }

    public void setUser_job_title(String user_job_title) {
        this.user_job_title = user_job_title;
    }

    public String getUser_district() {
        return user_district;
    }

    public void setUser_district(String user_district) {
        this.user_district = user_district;
    }

    public String getAttachment_id() {
        return attachment_id;
    }

    public void setAttachment_id(String attachment_id) {
        this.attachment_id = attachment_id;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
}
