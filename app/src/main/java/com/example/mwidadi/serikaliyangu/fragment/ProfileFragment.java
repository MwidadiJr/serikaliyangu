package com.example.mwidadi.serikaliyangu.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.AboutSerikaliYangu;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.profile.EditProfileActivity;
import com.example.mwidadi.serikaliyangu.userChallenge.UserChallengeActivity;
import com.example.mwidadi.serikaliyangu.userForum.ForumManagementActivity;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.LanguagePreference;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.example.mwidadi.serikaliyangu.utils.StringFormatter;
import com.squareup.picasso.Picasso;
import java.util.Locale;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private ImageView iv_profile;
    private TextView tv_name,tv_description,tv_location;
    private Button bn_edit_profile;
    private LinearLayout ll_setting,ll_share,ll_policy,ll_about;
    private PreferenceManager preferenceManager;
    private String first_name;
    private String last_name;
    private String middle_name;
    private String phone_number;
    private String email;
    private String gender;
    private String job_status;
    private String job_title;
    private String avatar;
    private String birth_day;
    private String user_id;
    private String full_avatar_url = "";
    private String share_text = "At vero eos et accusamus et iusto odio molestias excepturi";
    private StringFormatter stringFormatter;
    private String formatted_job_title;
    private String region_name,district_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        preferenceManager = new PreferenceManager(getContext());
        stringFormatter = new StringFormatter(getContext());
        getUserInfo();
        iv_profile = (ImageView)v.findViewById(R.id.iv_change_image);
        tv_name = (TextView)v. findViewById(R.id.tv_name);
        tv_description = (TextView)v. findViewById(R.id.tv_description);
        tv_location = (TextView)v. findViewById(R.id.tv_location);
        bn_edit_profile = (Button)v. findViewById(R.id.bn_edit_profile);
        ll_setting = (LinearLayout)v. findViewById(R.id.ll_setting);
        ll_about = (LinearLayout)v. findViewById(R.id.ll_about);
        ll_share = (LinearLayout) v.findViewById(R.id.ll_share);
        ll_policy = (LinearLayout)v. findViewById(R.id.ll_policy);
        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        bn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.no_change);
            }
        });
        ll_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                terms();
            }
        });
        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });
        ll_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutIntent = new Intent(getContext(), AboutSerikaliYangu.class);
                startActivity(aboutIntent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        ll_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSetting();
            }
        });
        setInfo();
        return v;
    }

    private void setInfo(){
        Picasso.with(this.getContext()).load(full_avatar_url)
                .placeholder(R.drawable.ic_action_avatar)
                .into(iv_profile);
        tv_name.setText(first_name+" "+middle_name+" "+last_name);
        tv_description.setText(formatted_job_title);
        tv_location.setText(region_name);
    }

    private void getUserInfo(){
        first_name = preferenceManager.getFirstName();
        middle_name = preferenceManager.getMiddleName();
        last_name = preferenceManager.getLastName();
        phone_number = preferenceManager.getPhone();
        email = preferenceManager.getEmail();
        gender = preferenceManager.getGender();
        birth_day = preferenceManager.getBirthDay();
        job_title = preferenceManager.getJobTitle();
        job_status = preferenceManager.getJobStatus();
        avatar = preferenceManager.getAvatar();
        user_id = preferenceManager.getUserId();
        full_avatar_url = APIConfig.PREPEND_STORAGE_URL+avatar;
        formatted_job_title = stringFormatter.properCase(job_title);
        region_name = stringFormatter.properCase(preferenceManager.getRegionName());
        district_name = stringFormatter.properCase(preferenceManager.getDistrictName());
    }

    private void showOption() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.text_my_posts));
        builder.setMessage(getString(R.string.text_my_post_info));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_forum_capital, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                Intent intent = new Intent(getContext(), ForumManagementActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.text_challenge_capital, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getContext(), UserChallengeActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void share() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,share_text);
        startActivity(Intent.createChooser(shareIntent, "Serikali Yangu share options"));
    }

    private void terms(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("TERMS AND POLICY");
        builder.setMessage(R.string.dummy_text);
        builder.setCancelable(false);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void showSetting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.text_chnage_language));
        builder.setMessage(getString(R.string.change_language_setting));
        builder.setIcon(R.drawable.ic_action_language);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.text_english, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                setCurrentLanguage("en");
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.text_swahili, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                setCurrentLanguage("sw");
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void setCurrentLanguage(String currentLanguage) {
        LanguagePreference languagePref = new LanguagePreference(getContext());
        languagePref.setLanguage(currentLanguage);
        Locale locale = new Locale(languagePref.getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
        getActivity().recreate();
    }
}
