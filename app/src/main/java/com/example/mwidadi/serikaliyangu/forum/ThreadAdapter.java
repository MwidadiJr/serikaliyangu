package com.example.mwidadi.serikaliyangu.forum;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class ThreadAdapter extends RecyclerView.Adapter<ThreadHolder> {
    Context context;
    ArrayList<ThreadModel> threadList = new ArrayList<>();

    public ThreadAdapter(Context context, ArrayList<ThreadModel> threadList){
        this.threadList = threadList;
        this.context = context;
    }

    @Override
    public ThreadHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_chat_list,parent,false);
        ThreadHolder thread = new ThreadHolder(v);
        return thread;
    }

    @Override
    public void onBindViewHolder(ThreadHolder holder, int position) {
        final ThreadModel thread = threadList.get(position);
        holder.name.setText(thread.getFirst_name()+" "+thread.getMiddle_name()+" "+thread.getLast_name());
        holder.message.setText(thread.getMessage());
        holder.time.setText(thread.getCreated_at());
        Picasso.with(this.context.getApplicationContext()).load(thread.getAvatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.avatar);
        holder.cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LeaderChatActivity.class);
                intent.putExtra("thread_item",thread);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.threadList.size();
    }
}
