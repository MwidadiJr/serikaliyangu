package com.example.mwidadi.serikaliyangu.chat;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mwidadi.serikaliyangu.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class UserAdapter extends RecyclerView.Adapter<UserHolder> {
    Context context;
    ArrayList<User> userList = new ArrayList<>();

    public UserAdapter(Context context, ArrayList<User> userList){
        this.userList = userList;
        this.context = context;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_user,parent,false);
        UserHolder userHolder = new UserHolder(v);
        return userHolder;
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
       final User user = userList.get(position);
        holder.name.setText(user.getFirst_name()+" "+user.getMiddle_name()+" "+user.getLast_name());
        holder.district.setText(user.getRegion_name()+" - "+user.getDistrict_name());
        Picasso.with(this.context.getApplicationContext()).load(user.getAvatar())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.avatar);
        holder.cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ChatActivity.class);
                intent.putExtra("chat_item",user);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.userList.size();
    }
}
