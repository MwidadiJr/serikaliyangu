package com.example.mwidadi.serikaliyangu.fragment;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.training.Training;
import com.example.mwidadi.serikaliyangu.training.TrainingAdapter;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainingFragment extends Fragment {

    private Toolbar toolbar;
    private String user_id;
    private RecyclerView rv_training;
    ArrayList<Training> trainingData = new ArrayList<>();
    private CharSequence timePassedString;
    private String ago_time;
    private ProgressBar pBar;
    private String district_id;
    private String training_id;
    private String training_title;
    private String training_post;
    private String training_created_at;
    private String training_updated_at;
    private String phone;
    private String email;
    private String file;
    private String user_created_at;
    private String user_updated_at;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String region_id;
    private String district_name;
    private String region_name;
    private String attachment_content;
    private String full_image_url = "";

    public TrainingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_training, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.training_label);
        rv_training = (RecyclerView) v.findViewById(R.id.rv_training);
        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        rv_training.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_training.setHasFixedSize(true);
        setHasOptionsMenu(true);
        getTraining();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.training_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()== R.id.action_refresh){
            if (pBar.getVisibility() == View.INVISIBLE){
                pBar.setVisibility(View.VISIBLE);
            }
           refresh();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getTraining(){
        pBar.setVisibility(View.VISIBLE);
        final StringRequest trainingRequest = new StringRequest(Request.Method.GET, APIConfig.GET_TRAINING_URL, new Response.Listener<String>() {
           JSONObject trainingObject = null;
            @Override
            public void onResponse(String response) {
                pBar.setVisibility(View.GONE);
                try {
                    trainingObject = new JSONObject(response);
                    JSONArray trainingArray = trainingObject.getJSONArray("trainings");
                    trainingData.clear();
                    for (int i = 0;i<trainingArray.length();i++){
                        JSONObject singleTrainingObject = trainingArray.getJSONObject(i);
                        Training training = new Training();
                        training_id = singleTrainingObject.getString("id");
                        user_id = singleTrainingObject.getString("user_id");
                        training_title = singleTrainingObject.getString("title");
                        training_post = singleTrainingObject.getString("post");
                        training_created_at = singleTrainingObject.getString("created_at");
                        training_updated_at = singleTrainingObject.getString("updated_at");
                        file = singleTrainingObject.getString("file");

                        full_image_url = APIConfig.PREPEND_STORAGE_URL+file;

                        CharSequence formatedTime = formatTime(training_created_at);
                        ago_time = formatedTime.toString();

                        JSONObject userObject = singleTrainingObject.getJSONObject("user");
                        user_id = userObject.getString("id");
                        phone = userObject.getString("phone");
                        email = userObject.getString("email");
                        user_created_at = userObject.getString("created_at");
                        user_updated_at = userObject.getString("updated_at");

                        JSONObject profileObject = userObject.getJSONObject("profile");
                        profile_id = profileObject.getString("id");
                        user_id = profileObject.getString("user_id");
                        district_id = profileObject.getString("district_id");
                        first_name = profileObject.getString("first_name");
                        middle_name = profileObject.getString("middle_name");
                        last_name = profileObject.getString("last_name");
                        user_avatar = profileObject.getString("avatar");

                        JSONObject districtObject = profileObject.getJSONObject("district");
                        district_id = districtObject.getString("id");
                        region_id = districtObject.getString("region_id");
                        district_name = districtObject.getString("name");

                        JSONObject regionObject = districtObject.getJSONObject("region");
                        region_name = regionObject.getString("name");

                        training.setTraining_title(training_title);
                        training.setTraining_post(training_post);
                        training.setTraining_id(training_id);
                        training.setCreated_at(ago_time);
                        training.setUser_updated_at(user_updated_at);
                        training.setUpdated_at(training_updated_at);
                        training.setUser_phone(phone);
                        training.setUser_email(email);
                        training.setUser_id(user_id);
                        training.setUser_created_at(user_created_at);
                        training.setProfile_id(profile_id);
                        training.setFirst_name(first_name);
                        training.setMiddle_name(middle_name);
                        training.setLast_name(last_name);
                        training.setUser_avatar(user_avatar);
                        training.setDistrict_id(district_id);
                        training.setRegion_id(region_id);
                        training.setDistrict_name(district_name);
                        training.setRegion_name(region_name);
                        training.setAttachment_content(full_image_url);
                        trainingData.add(training);
                    }

                    TrainingAdapter adapter = new TrainingAdapter(getContext(), trainingData);
                    rv_training.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                Snackbar.make(rv_training, R.string.no_internet, Snackbar.LENGTH_INDEFINITE).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getTraining();
                    }
                }).show();
                pBar.setVisibility(View.INVISIBLE);
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        trainingRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(trainingRequest);
    }

    private CharSequence formatTime(String dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }

    public void refresh() {
        if (this.trainingData != null) {
            this.trainingData.clear();
            getTraining();
            return;
        }
        else{
            getTraining();
        }
    }

}
