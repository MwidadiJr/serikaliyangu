package com.example.mwidadi.serikaliyangu.utils;

import android.content.Context;

/**
 * Created by Mwidadi on 11/30/2017.
 */

public class StringFormatter {
    private Context mContext;

    public StringFormatter(Context context){
        this.mContext = context;
    }

    public String properCase(String paramString) {
        if (paramString.length() == 0) {
            return "";
        }
        if (paramString.length() == 1) {
            return paramString.toUpperCase();
        }
        return paramString.substring(0, 1).toUpperCase() + paramString.substring(1).toLowerCase();
    }
}
