package com.example.mwidadi.serikaliyangu.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.forum.Forum;
import com.example.mwidadi.serikaliyangu.forum.ForumAdapter;
import com.example.mwidadi.serikaliyangu.profile.ProfileActivity;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.APIPostRequest;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends Fragment {

    private Context mContext;
    private RecyclerView rv_forum;
    private String forum_id;
    private String forum_title;
    private String forum_post;
    private String forum_attachment;
    private String forum_created_at;
    private String forum_updated_at;
    private String phone;
    private String email;
    private String user_created_time;
    private String user_updated_time;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String dob;
    private String gender;
    private String marital_status;
    private String job_status;
    private String job_title;
    private String full_image_path = "";
    private ProgressBar pBar;
    private Button bn_send;
    private EditText et_name,et_title;
    ArrayList<Forum> forumData = new ArrayList<>();
    private ForumAdapter adapter;
    private CharSequence timePassedString;
    private String ago_time;
    private Toolbar toolbar;
    private APIPostRequest apiPostRequest;
    private PreferenceManager preferenceManager;
    private String user_id;
    private String received_user_id;
    private Forum forum;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_forum, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        calligrapher.setFont(getActivity(),"Avenir-Medium.ttf",true);
        setHasOptionsMenu(true);
        checkInternetConnection();
        rv_forum = (RecyclerView) v.findViewById(R.id.rv_forum);
        rv_forum.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.forum_label);
        getForum(APIConfig.GET_FORUM_URL);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        received_user_id = preferenceManager.getUserId();
        apiPostRequest = new APIPostRequest(mContext);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.forum_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_create) {
              getDialog();
        }
        else if (item.getItemId() == R.id.action_profile){
            Intent intent = new Intent(mContext,ProfileActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        }
        else if (item.getItemId() == R.id.action_refresh){
            refresh();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getForum(final String get_forum_url) {
        StringRequest forumRequest = new StringRequest(Request.Method.GET, get_forum_url, new Response.Listener<String>() {
            JSONObject forumObject = null;

            @Override
            public void onResponse(String response) {
                pBar.setVisibility(View.GONE);
                try {
                    forumObject = new JSONObject(response);
                    forumData.clear();
                    JSONArray forumArray = forumObject.getJSONArray("forums");
                    for (int i = 0; i < forumArray.length(); i++) {
                        forumObject = forumArray.getJSONObject(i);
                        forum = new Forum();
                        forum_id = forumObject.getString("id");
                        user_id = forumObject.getString("user_id");
                        forum_title = forumObject.getString("title");
                        forum_post = forumObject.getString("post");
                        forum_attachment = forumObject.getString("attachment");
                        forum_created_at = forumObject.getString("created_at");
                        forum_updated_at = forumObject.getString("updated_at");

                        JSONObject userObject = forumObject.optJSONObject("user");
                        user_id = userObject.getString("id");
                        phone = userObject.getString("phone");
                        email = userObject.getString("email");
                        user_created_time = userObject.getString("created_at");
                        user_updated_time = userObject.getString("updated_at");

                        JSONObject profileObject = userObject.getJSONObject("profile");
                        profile_id = profileObject.getString("id");
                        user_id = profileObject.getString("user_id");
                        first_name = profileObject.getString("first_name");
                        middle_name = profileObject.getString("middle_name");
                        last_name = profileObject.getString("last_name");
                        user_avatar = profileObject.getString("avatar");
                        dob = profileObject.getString("dob");
                        gender = profileObject.getString("gender");
                        marital_status = profileObject.getString("marital_status");
                        job_status = profileObject.getString("job_status");
                        job_title = profileObject.getString("job_title");
                        full_image_path = APIConfig.PREPEND_STORAGE_URL + user_avatar;

                        CharSequence formatedTime = formatTime(forum_created_at);
                        ago_time = formatedTime.toString();

                        forum.setForum_id(forum_id);
                        forum.setForum_title(forum_title);
                        forum.setForum_content(forum_post);
                        forum.setForum_attachment(forum_attachment);
                        forum.setPosted_time(ago_time);
                        forum.setUser_id(user_id);
                        forum.setFirst_name(first_name);
                        forum.setMiddle_name(middle_name);
                        forum.setLast_name(last_name);
                        forum.setUser_avatar(full_image_path);
                        forumData.add(forum);

                    }

                    adapter = new ForumAdapter(getContext(), forumData);
                    rv_forum.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBar.setVisibility(View.GONE);
                Snackbar.make(rv_forum, R.string.no_internet, Snackbar.LENGTH_INDEFINITE).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getForum(APIConfig.GET_FORUM_URL);
                    }
                }).show();
                pBar.setVisibility(View.INVISIBLE);
            }
        });

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        forumRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(forumRequest);
    }

    private CharSequence formatTime(String dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = df.parse(dateTime);
            long time = date.getTime();
            timePassedString = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timePassedString;
    }

    private boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Toast.makeText(getContext(), "No Network Available", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void getDialog(){
        final Dialog createDialog = new Dialog(getContext());
        createDialog.setContentView(R.layout.forum_dialog);
        bn_send = (Button) createDialog.findViewById(R.id.bn_send);
        et_name = (EditText) createDialog.findViewById(R.id.tv_forum_name);
        et_title = (EditText) createDialog.findViewById(R.id.tv_forum_title);

        bn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String forum_title = et_title.getText().toString().trim();
                String forum_content = et_name.getText().toString().trim();
                if (TextUtils.isEmpty(forum_content) || TextUtils.isEmpty(forum_title)){
                    Toast.makeText(mContext,"Cannot be empty",Toast.LENGTH_SHORT).show();
                }
                else {
                  sendForum();
                }
                createDialog.dismiss();
                refresh();
            }
        });

        createDialog.show();
    }


    public void createForum(final String user_id,final String title,final String post,final String attachment,String forum_url){
        StringRequest forumRequest = new StringRequest(Request.Method.POST, forum_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               if (response.contains("saved")){
                   Toast.makeText(mContext,"Forum created",Toast.LENGTH_SHORT).show();
               }
               else {
                   Toast.makeText(mContext,"Failed please try again",Toast.LENGTH_SHORT).show();
                   getDialog();
               }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getActivity(). getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getActivity());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_TITLE,title);
                params.put(APIConfig.KEY_POST,post);
                params.put(APIConfig.KEY_ATTACHMENT,attachment);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        forumRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(forumRequest);
    }

    private void sendForum(){
       createForum(received_user_id,et_title.getText().toString().trim(),et_name.getText().toString().trim(),"",APIConfig.CREATE_FORUM_URL);
    }

    public void refresh() {
        if (this.forumData != null) {
            this.forumData.clear();
            getForum(APIConfig.GET_FORUM_URL);
            return;
        }
        else{
            getForum(APIConfig.GET_FORUM_URL);
        }
    }

}
