package com.example.mwidadi.serikaliyangu.training;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.example.mwidadi.serikaliyangu.utils.PreferenceManager;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseHolder> {
    Context context;
    ArrayList<CourseModel> courseList = new ArrayList<>();
    private PreferenceManager preferenceManager;
    private String user_id;

    public CourseAdapter(Context context, ArrayList<CourseModel> courseList){
        this.courseList = courseList;
        this.context = context;
        preferenceManager = new PreferenceManager(context);
        user_id = preferenceManager.getUserId();
    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_course,parent,false);
        CourseHolder holder = new CourseHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(CourseHolder holder, int position) {
       final CourseModel course = courseList.get(position);
        holder.title.setText(course.getTitle());
        holder.time.setText(course.getCreated_at());
        Picasso.with(this.context.getApplicationContext()).load(course.getCover_image())
                .placeholder(R.drawable.ic_action_avatar)
                .into(holder.cover_image);
        holder.bn_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CourseDetailActivity.class);
                intent.putExtra("course_item",course);
                takeLesson(user_id,course.getCourse_id());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.courseList.size();
    }

    private void takeLesson(final String user_id, final String course_id){
        StringRequest lessonRequest = new StringRequest(Request.Method.POST, APIConfig.ASSIGN_COURSE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(APIConfig.KEY_USER_ID,user_id);
                params.put(APIConfig.KEY_COURSE_ID,course_id);
                return params;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        lessonRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(lessonRequest);
    }

}
