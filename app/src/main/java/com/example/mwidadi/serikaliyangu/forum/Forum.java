package com.example.mwidadi.serikaliyangu.forum;

import java.io.Serializable;

/**
 * Created by Mwidadi on 10/22/2017.
 */

public class Forum implements Serializable{
   private String forum_id;
    private String user_id;
    private String forum_title;
    private String forum_content;
    private String forum_attachment;
    private String posted_time;
    private String updated_time;
    private String user_phone;
    private String user_email;
    private String user_created_time;
    private String user_updated_time;
    private String profile_id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String user_avatar;
    private String user_dob;
    private String user_gender;
    private String user_marital_status;
    private String user_job_status;
    private String job_title;

    public String getForum_id() {
        return forum_id;
    }

    public void setForum_id(String forum_id) {
        this.forum_id = forum_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getForum_title() {
        return forum_title;
    }

    public void setForum_title(String forum_title) {
        this.forum_title = forum_title;
    }

    public String getForum_content() {
        return forum_content;
    }

    public void setForum_content(String forum_content) {
        this.forum_content = forum_content;
    }

    public String getForum_attachment() {
        return forum_attachment;
    }

    public void setForum_attachment(String forum_attachment) {
        this.forum_attachment = forum_attachment;
    }

    public String getPosted_time() {
        return posted_time;
    }

    public void setPosted_time(String posted_time) {
        this.posted_time = posted_time;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_created_time() {
        return user_created_time;
    }

    public void setUser_created_time(String user_created_time) {
        this.user_created_time = user_created_time;
    }

    public String getUser_updated_time() {
        return user_updated_time;
    }

    public void setUser_updated_time(String user_updated_time) {
        this.user_updated_time = user_updated_time;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_dob() {
        return user_dob;
    }

    public void setUser_dob(String user_dob) {
        this.user_dob = user_dob;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_marital_status() {
        return user_marital_status;
    }

    public void setUser_marital_status(String user_marital_status) {
        this.user_marital_status = user_marital_status;
    }

    public String getUser_job_status() {
        return user_job_status;
    }

    public void setUser_job_status(String user_job_status) {
        this.user_job_status = user_job_status;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }
}
