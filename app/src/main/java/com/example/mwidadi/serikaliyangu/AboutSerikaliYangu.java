package com.example.mwidadi.serikaliyangu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import me.anwarshahriar.calligrapher.Calligrapher;

public class AboutSerikaliYangu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_serikali_yangu);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
    }

}
