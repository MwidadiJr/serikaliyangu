package com.example.mwidadi.serikaliyangu.training;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.utils.APIConfig;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import me.anwarshahriar.calligrapher.Calligrapher;

public class CourseDetailActivity extends YouTubeBaseActivity implements AppCompatCallback,View.OnClickListener {

    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private AppCompatDelegate delegate;
    private TextView tv_title;
    private ImageView iv_add_comment;
    private ImageView iv_read_comment;
    private ImageView iv_share;
    private TextView tv_add_comment;
    private TextView tv_read_comment;
    private TextView tv_count;
    private TextView tv_description;
    private CourseModel course;
    private String title;
    private String description;
    private String video_link;
    private String course_id;
    private String video_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delegate = AppCompatDelegate.create(this, this);
        setContentView(R.layout.activity_course_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        delegate.setSupportActionBar(toolbar);
        delegate.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.no_change, R.anim.slide_down);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        initView();
        getInfo();
        setInfo();
        getCourseComment();

        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(video_id);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

        youTubePlayerView.initialize(getString(R.string.youtube_key),onInitializedListener);
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_description = (TextView) findViewById(R.id.tv_description);
        iv_add_comment = (ImageView) findViewById(R.id.iv_add_comment);
        iv_read_comment = (ImageView) findViewById(R.id.iv_read_comment);
        iv_share = (ImageView) findViewById(R.id.iv_share);
        tv_add_comment = (TextView) findViewById(R.id.tv_add_comment);
        tv_read_comment = (TextView) findViewById(R.id.tv_read_comment);
        tv_count = (TextView) findViewById(R.id.tv_count);
        iv_add_comment.setOnClickListener(this);
        iv_read_comment.setOnClickListener(this);
        iv_share.setOnClickListener(this);
        tv_read_comment.setOnClickListener(this);
        tv_add_comment.setOnClickListener(this);
    }

    private void getInfo() {
        course = (CourseModel) getIntent().getSerializableExtra("course_item");
        course_id = course.getCourse_id();
        title = course.getTitle();
        description = course.getDescription();
        video_link = course.getLink();
        video_id = video_link.substring(video_link.lastIndexOf("=") + 1);
        delegate.getSupportActionBar().setTitle(title);
    }

    private void setInfo() {
        tv_title.setText(title);
        tv_description.setText(description);
    }


    private void getCourseComment() {
        StringRequest commentRequest = new StringRequest(Request.Method.GET, APIConfig.GET_COURSE_COMMENT_URL + course_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject commentObject = new JSONObject(response);
                    JSONObject singleCommentObject = commentObject.getJSONObject("course");
                    JSONArray commentArray = singleCommentObject.getJSONArray("comments");
                    tv_count.setText(String.valueOf(commentArray.length()));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof com.android.volley.TimeoutError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_timeout, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();

                }
                else if (error instanceof com.android.volley.AuthFailureError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_autherror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.NetworkError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_networkerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.ServerError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_servererror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof com.android.volley.ParseError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_parseerror, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
                else if (error instanceof  com.android.volley.NoConnectionError){
                    LayoutInflater inflater = getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(toastLayout);
                    toast.show();
                }
            }
        }) {

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(commentRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add_comment:
                Intent commentIntent = new Intent(getApplicationContext(), TrainingCommentActivity.class);
                commentIntent.putExtra("course_id", course_id);
                startActivity(commentIntent);
                break;

            case R.id.iv_read_comment:
                Intent readTextIntent = new Intent(getApplicationContext(), ReadCommentActivity.class);
                readTextIntent.putExtra("course_id", course_id);
                startActivity(readTextIntent);
                break;

            case R.id.iv_share:
                Toast.makeText(getApplicationContext(), "Share", Toast.LENGTH_SHORT).show();
                break;

            case R.id.tv_add_comment:
                Intent commentTextIntent = new Intent(getApplicationContext(), TrainingCommentActivity.class);
                commentTextIntent.putExtra("course_id", course_id);
                startActivity(commentTextIntent);
                break;

            case R.id.tv_read_comment:
                Intent readImageIntent = new Intent(getApplicationContext(), ReadCommentActivity.class);
                readImageIntent.putExtra("course_id", course_id);
                startActivity(readImageIntent);
                break;

            default:
                Toast.makeText(getApplicationContext(), "Default Action", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {

    }

    @Override
    public void onSupportActionModeFinished(ActionMode mode) {

    }

    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }
}
