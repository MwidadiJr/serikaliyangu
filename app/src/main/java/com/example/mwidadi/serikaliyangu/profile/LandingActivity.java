package com.example.mwidadi.serikaliyangu.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.example.mwidadi.serikaliyangu.R;
import com.example.mwidadi.serikaliyangu.registration.BasicRegisterActivity;
import me.anwarshahriar.calligrapher.Calligrapher;

public class LandingActivity extends AppCompatActivity implements View.OnClickListener{
    private Button bn_login,bn_register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        bn_login = (Button) findViewById(R.id.bn_login);
        bn_register = (Button) findViewById(R.id.bn_register);
        bn_login.setOnClickListener(this);
        bn_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == bn_login){
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        }
        else if (v == bn_register){
            Intent intent = new Intent(getApplicationContext(),BasicRegisterActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        }
    }

}
