package com.example.mwidadi.serikaliyangu.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.example.mwidadi.serikaliyangu.R;
import com.github.library.bubbleview.BubbleTextView;
import java.util.List;

/**
 * Created by Mwidadi on 12/20/2017.
 */

public class ChatCustomAdpater extends BaseAdapter {

    private List<ChatModel> lstChat;
    private Context context;
    private LayoutInflater inflater;

    public ChatCustomAdpater(List<ChatModel> lstChat, Context context) {
        this.lstChat = lstChat;
        this.context = context;
       inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lstChat.size();
    }

    @Override
    public Object getItem(int position) {
        return lstChat.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (convertView ==  null){
            if (lstChat.get(position).isSend())
            v = inflater.inflate(R.layout.list_send,null);
            else
            v = inflater.inflate(R.layout.list_receiverr,null);
        }

        BubbleTextView bubbleTextView = (BubbleTextView)v.findViewById(R.id.message_text_layout);
        bubbleTextView.setText(lstChat.get(position).chatMessage);
        return v;
    }
}
