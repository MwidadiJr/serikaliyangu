package com.example.mwidadi.serikaliyangu.blog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.internal.NavigationMenu;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.florent37.diagonallayout.DiagonalLayout;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.FileOutputStream;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import me.anwarshahriar.calligrapher.Calligrapher;

public class BlogDetailActivity extends AppCompatActivity {

    private DiagonalLayout dl_main;
    private KenBurnsView kb_main;
    private CircleImageView iv_author;
    private TextView tv_title;
    private TextView tv_time;
    private TextView tv_author;
    private TextView tv_content;
    private BlogPost post;
    private String news_id;
    private String news_title;
    private String news_content;
    private String posted_time;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String author;
    private String post_image;
    private String author_image;
    private NestedScrollView nl_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.forum_detail_label);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                }
            });
        }
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this,"Avenir-Medium.ttf",true);
        initView();
        setUpFab();
        getPost();
        setText();
    }

    private void initView(){
        dl_main = (DiagonalLayout) findViewById(R.id.diagonalLayout);
        kb_main = (KenBurnsView) findViewById(R.id.kb_main);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_author = (TextView) findViewById(R.id.tv_author);
        iv_author = (CircleImageView) findViewById(R.id.iv_author);
        tv_content = (TextView) findViewById(R.id.tv_content);
        nl_main = (NestedScrollView)findViewById(R.id.nl_main);
    }

    private void getPost(){
        post = (BlogPost) getIntent().getSerializableExtra("news_item");
        news_id = post.getPost_id();
        news_title = post.getPost_title();
        news_content = post.getPost_content();
        posted_time = post.getPost_created();
        first_name = post.getFirst_name();
        middle_name = post.getMiddle_name();
        last_name = post.getLast_name();
        author = first_name;
        post_image = post.getAttachment();
        author_image = post.getUser_avatar();

    }

    private void setText(){
        tv_content.setText(news_content);
        tv_author.setText(author);
        tv_time.setText(posted_time);
        tv_title.setText(news_title);
        Picasso.with(getApplicationContext()).load(post_image)
                .placeholder(R.drawable.ic_action_avatar)
                .into(kb_main);

        Picasso.with(getApplicationContext()).load(author_image)
                .placeholder(R.drawable.ic_action_avatar)
                .into(iv_author);
    }

    private void setUpFab(){
        FabSpeedDial dial = (FabSpeedDial) findViewById(R.id.fab_option);
        dial.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                if (menuItem.getTitle().equals("Comment")){
                    Intent commentIntent = new Intent(getApplicationContext(),BlogCommentActivity.class);
                    commentIntent.putExtra("post_id",news_id);
                    startActivity(commentIntent);
                    overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                }
                else if (menuItem.getTitle().equals("Share")){
                    share();
                }
                else {

                }
                return true;
            }

            @Override
            public void onMenuClosed() {

            }
        });
    }

    private void share(){
        Bitmap bitmap = getBitmapFromView(nl_main);
        try {
            File file = new File(getCacheDir(), "serikaliyangublog" + ".png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share news post via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null) {
            bgDrawable.draw(canvas);
        }   else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }
}

