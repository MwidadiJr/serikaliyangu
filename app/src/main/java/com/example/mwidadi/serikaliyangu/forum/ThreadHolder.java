package com.example.mwidadi.serikaliyangu.forum;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mwidadi.serikaliyangu.R;

/**
 * Created by Mwidadi on 8/14/2017.
 */

public class ThreadHolder extends RecyclerView.ViewHolder {
    ImageView avatar;
    TextView name;
    TextView message;
    TextView time;
    CardView cvCard;

    public ThreadHolder(View itemView) {
        super(itemView);
        avatar = (ImageView) itemView.findViewById(R.id.iv_avatar);
        name = (TextView) itemView.findViewById(R.id.tv_name);
        message = (TextView) itemView.findViewById(R.id.tv_message);
        time = (TextView) itemView.findViewById(R.id.tv_time);
        cvCard = (CardView) itemView.findViewById(R.id.cv_thread);
    }
}
