package com.example.mwidadi.serikaliyangu.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class CameraPreference
{
    private static final String CAMERA_ALLOWED = "ALLOWED";
    private static final String CAMERA_PREF_NAME = "CAMERA_ALLOWED";
    private int PRIVATE_MODE = 0;
    Context context;
    SharedPreferences.Editor editor;
    SharedPreferences preferences;

    public CameraPreference(Context paramContext)
    {
        this.context = paramContext;
        this.preferences = paramContext.getSharedPreferences("CAMERA_ALLOWED", this.PRIVATE_MODE);
        this.editor = this.preferences.edit();
    }

    public Boolean getCameraAllowed()
    {
        return Boolean.valueOf(this.preferences.getBoolean("ALLOWED", false));
    }

    public void setCameraAllowed(boolean paramBoolean)
    {
        this.editor.putBoolean("ALLOWED", paramBoolean);
        this.editor.commit();
    }
}

